# Sumário Executivo Milestone1

## Modelo de domínio MS1

O modelo de domínio do projeto está representado na imagem abaixo:
![Domain Model](./md_ms1.png)

Na eleboração do modelo de domínio deste projeto foi tida em conta a estrutura do ficheiro de xml de entrada de forma a facilitar a leitura dos valores dos elementso dos ficheiros xml.
A desvantagem deste tipo de modelo de domínio é a repetição da implementação para os Teachers e para os Externals. Uma alternativa de implementação seria a utilização do tipo Resource em vez dos tipos Teacher e ExternalPerson na classe Viva.

## Agoritmo de escalonamento dos Vivas

Para a elaboração do algoritmo de escalonamento dos vivas deve ser considerado a primeira data para a qual todos estão disponíveis.

.......

Para saber qual a data para a qual todos os recursos estão disponíveis foi utilizado um algoritmo disponível em https://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap de modo a obter a sobreposição temporal das disponibilidades dos recursos.
Esse algoritmo encontra-se resumido na seguinte imagem:

![Overlap](./overlap.jpg)