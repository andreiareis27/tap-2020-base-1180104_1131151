package assessment

import java.time.temporal.ChronoUnit
import java.time.{LocalDateTime, LocalTime}

import domain.schedule._
import org.scalacheck.Prop.forAll
import org.scalacheck.{Gen, Properties}

import scala.annotation.tailrec
import scala.util.{Success, Try}

object SchedulePropertyTest extends Properties("Schedule") {

  def genNonEmptyString(start: String): Gen[NonEmptyString] = for {
    s <- Gen.listOfN(10, Gen.alphaChar)
    if !s.mkString("").isEmpty
  } yield NonEmptyString.from(start + s.mkString(""), "").getOrElse(???)

  def convertDiffToInst(start: LocalDateTime, ld: List[Int]): List[LocalDateTime] =
    ld.foldLeft[(LocalDateTime, List[LocalDateTime])](start, Nil) { case ((i, li), d) =>
      val ni = i.plus(d, ChronoUnit.MINUTES)
      (ni, li :+ ni)
    }._2

  def genInstants(start: LocalDateTime): Gen[List[LocalDateTime]] = for {
    n <- Gen.chooseNum(minT = 0, maxT = 9)
    //n <- Gen.chooseNum(minT = 1, maxT = 9)
    ld <- Gen.listOfN(n, Gen.chooseNum(minT = 10, maxT = 60))
    //ld <- Gen.listOfN(n, Gen.chooseNum(minT = 120, maxT = 420))
  } yield convertDiffToInst(start, 0 :: ld)

  def genInstant: Gen[LocalDateTime] = for {
    instants <- genInstants(LocalDateTime.now())
    instant <- Gen.oneOf(instants)
  } yield instant

  def genInstantEnd: Gen[LocalDateTime] = for {
    instants <- genInstants(LocalDateTime.now().plusMinutes(15))
    instant <- Gen.oneOf(instants)
  } yield instant

  def genAvailability: Gen[Try[Availability]] = for {
    start <- genInstant
    end <- genInstant
    //end<-genInstantEnd
    pref <- Gen.choose[Int](1, 5)
  } yield Availability.from(start, end, pref)


   def genTeacher: Gen[Teacher] = for {
    id <- genNonEmptyString("T")
    name <- genNonEmptyString("Teacher ")
    n <- Gen.chooseNum(1, 9)
    la <- Gen.listOfN(n, genAvailability)
  } yield Teacher(id, name, la.filter(_.isSuccess).map(_.get))



  def genExternal: Gen[ExternalPerson] = for {
    id <- genNonEmptyString("E")
    name <- genNonEmptyString("External ")
    n <- Gen.chooseNum(1, 9)
    la <- Gen.listOfN(n, genAvailability)
  } yield ExternalPerson(id, name, la.filter(_.isSuccess).map(_.get))

 /* def genExternal: Gen[ExternalPerson] = for {
    ep<-genTryExternal
    if(ep.isSuccess)
  } yield ep.getOrElse(???)*/

  def genTeachers: Gen[Teachers] = for {
    n <- Gen.chooseNum(1, 9)
    lt <- Gen.listOfN(n, genTeacher)
  } yield Teachers(lt)

  def genExternals: Gen[Externals] = for {
    n <- Gen.chooseNum(1, 9)
    lt <- Gen.listOfN(n, genExternal)
  } yield Externals(lt)

  def genResources: Gen[Resources] = for {
    resT <- genTeachers
    resE <- genExternals
  } yield Resources(resT, resE)


  @scala.annotation.tailrec
  def genCodAdviserT(tRes: List[Teacher], result: List[Teacher], index: Int): List[Teacher] = {
    if (index != result.size) {
      tRes match {
        case Nil => result
        case h :: tail => genCodAdviserT(tail, h :: result, index + 1)
      }
    } else {
      result
    }
  }

  @scala.annotation.tailrec
  def genCodAdviserE(tRes: List[ExternalPerson], result: List[ExternalPerson], index: Int):List[ExternalPerson]= {
    if (index != result.size) {
      tRes match {
        case Nil => result
        case h :: tail => genCodAdviserE(tail, h :: result, index + 1)
      }
    } else {
      result
    }
  }

  def genVivaRes(res: Resources): Gen[Try[Viva]] = for {
    student <- genNonEmptyString("Student ")
    title <- genNonEmptyString("Title ")

    president <- Gen.oneOf(res.teachers.teacherList)
    adviser <- Gen.oneOf(res.teachers.teacherList)

    nCodAT <- Gen.chooseNum(0, res.teachers.teacherList.size)
    codAdviserT = genCodAdviserT(res.teachers.teacherList, List(), nCodAT)

    nCodAE: Int <- Gen.chooseNum(0, res.externals.externalList.size)
    codAdviserE = genCodAdviserE(res.externals.externalList, List(), nCodAE)

    nSuper <- Gen.chooseNum(0, res.externals.externalList.size)
    superVisor = genCodAdviserE(res.externals.externalList, List(), nSuper)

  } yield Viva.from(student, title, president, adviser, codAdviserT++codAdviserE, Externals(superVisor))
  //yield Viva.from(student, title, Try(president), Try(adviser), Resources(codAdviserT, codAdviserE), superVisor)

  def genVivasResources(res: Resources): Gen[List[Try[Viva]]] = for {
    n <- Gen.chooseNum(1, 9)
    lt <- Gen.listOfN(n, genVivaRes(res))
  } yield lt

  def genAgendaDuration(res: Resources, vivas: Vivas): Gen[Try[Agenda]] = for {
    hour <- Gen.chooseNum(0, 9)
    //hour <- Gen.chooseNum(0, 1)
    minute <- Gen.chooseNum(1, 59)
  } yield Agenda.from(vivas, res, LocalTime.of(hour, minute))
  //yield Agenda.from(Try(vivas), Try(res), LocalTime.of(hour, minute))


  def checkInstance(resourcesList:List[Resource]): Resources ={

    @scala.annotation.tailrec
    def instance(r:List[Resource], t:List[Teacher], e:List[ExternalPerson]):Resources={
      r match {
        case h::tail=> h match {
          case Teacher(id,name,availability)=>{
            instance(tail,t:+Teacher(id,name,availability),e)
          }
          case ExternalPerson(id,name,availability)=>{
            instance(tail,t,e:+ExternalPerson(id,name,availability))
          }
        }
        case  Nil=> Resources(Teachers(t),Externals(e))
      }
    }

    instance(resourcesList,List(),List())

  }


  def getResourcesFromSV(scheduledViva: ScheduledViva): Resources = {
    val coAdvisersViva: List[Resource] =scheduledViva.viva.coAdvisers
    val getTeachersPresAdv: List[Teacher] = List(scheduledViva.viva.president, scheduledViva.viva.adviser)
    val getTeachersViva: List[Teacher] = checkInstance(coAdvisersViva).teachers.teacherList ++ getTeachersPresAdv
    val getExternalsViva: List[ExternalPerson] =
      checkInstance(coAdvisersViva).externals.externalList ++ scheduledViva.viva.supervisors.externalList
    Resources(Teachers(getTeachersViva), Externals(getExternalsViva))
  }

  def getResourcesFromViva(viva: Viva): Resources = {
    val coAdvisersViva=viva.coAdvisers
    val getTeachersPresAdv: List[Teacher] = List(viva.president, viva.adviser)
    val getTeachersViva: List[Teacher] = checkInstance(coAdvisersViva).teachers.teacherList ++ getTeachersPresAdv
    val getExternalsViva: List[ExternalPerson] =
      checkInstance(coAdvisersViva).externals.externalList ++ viva.supervisors.externalList
    Resources(Teachers(getTeachersViva), Externals(getExternalsViva))
  }

  def compareScheduleWithAvail(sh: ScheduledViva, v: Viva): Boolean = {


    val coAdvisersViva: List[Resource] =v.coAdvisers

    val avlPresident: List[Availability] = v.president.availability
    val avlAdviser: List[Availability] = v.adviser.availability
    val avlE: List[Availability] = checkInstance(coAdvisersViva).externals.externalList.flatMap(x => x.availability)
    val avlT: List[Availability] = checkInstance(coAdvisersViva).teachers.teacherList.flatMap(x => x.availability)
    val avlCodAdv: List[Availability] = avlE ++ avlT
    val avlSuper: List[Availability] = v.supervisors.externalList.flatMap(x => x.availability)

    val allAvl: List[Availability] = avlPresident ++ avlAdviser ++ avlCodAdv ++ avlSuper

    if (!avlPresident.exists(x => (sh.start.isAfter(x.start) || sh.start.isEqual(x.start)) &&
      (sh.end.isBefore(x.end) || sh.end.isEqual(x.end)))) {
      false
    } else if (!avlAdviser.exists(x => (sh.start.isAfter(x.start) || sh.start.isEqual(x.start)) &&
      (sh.end.isBefore(x.end) || sh.end.isEqual(x.end)))) {
      false
    } else if (avlCodAdv.nonEmpty && !avlCodAdv.exists(x => (sh.start.isAfter(x.start) || sh.start.isEqual(x.start)) &&
      (sh.end.isBefore(x.end) || sh.end.isEqual(x.end)))) {
      false
    } else if (avlSuper.nonEmpty && !avlSuper.exists(x => (sh.start.isAfter(x.start) || sh.start.isEqual(x.start)) &&
      (sh.end.isBefore(x.end) || sh.end.isEqual(x.end)))) {
      false
    } else {
      true
    }
  }

  def compareScheduleWithAvl(sh: ScheduledViva): Boolean ={

    val allResources = getResourcesFromSV(sh)

    val teachersViva = allResources.teachers.teacherList
    val externalsViva = allResources.externals.externalList

    val availableTeachers = teachersViva.filter(t => t.availability.exists(x =>
      (sh.start.isAfter(x.start) || sh.start.isEqual(x.start)) &&
        (sh.end.isBefore(x.end) || sh.end.isEqual(x.end))))

    val availableExternals = externalsViva.filter(e => e.availability.exists(x =>
      (sh.start.isAfter(x.start) || sh.start.isEqual(x.start)) &&
        (sh.end.isBefore(x.end) || sh.end.isEqual(x.end))))

    val check: Boolean = (teachersViva.size == availableTeachers.size) && (externalsViva.size == availableExternals.size)
    check
  }

  def checkIfResAreUpdated(sh: ScheduledViva, v: Viva): Boolean = {
    val coAdvisersViva: List[Resource] =v.coAdvisers
    val avlPresident: List[Availability] = v.president.availability
    val avlAdviser: List[Availability] = v.adviser.availability
    val avlEt: List[Availability] = checkInstance(coAdvisersViva).externals.externalList.flatMap(x => x.availability)
    val avlTh: List[Availability] = checkInstance(coAdvisersViva).teachers.teacherList.flatMap(x => x.availability)
    val avlCodAdv: List[Availability] = avlEt ++ avlTh
    val avlPres: List[Availability] = v.supervisors.externalList.flatMap(x => x.availability)

    if (avlPresident.exists(x => (sh.start.isAfter(x.start) || sh.start.isEqual(x.start)) &&
      (sh.end.isBefore(x.end) || sh.end.isEqual(x.end)))) {
      false
    } else if (avlAdviser.exists(x => (sh.start.isAfter(x.start) || sh.start.isEqual(x.start)) &&
      (sh.end.isBefore(x.end) || sh.end.isEqual(x.end)))) {
      false
    } else if (avlCodAdv.nonEmpty && avlCodAdv.exists(x => (sh.start.isAfter(x.start) || sh.start.isEqual(x.start)) &&
      (sh.end.isBefore(x.end) || sh.end.isEqual(x.end)))) {
      false
    } else if (avlPres.nonEmpty && avlPres.exists(x => (sh.start.isAfter(x.start) || sh.start.isEqual(x.start)) &&
      (sh.end.isBefore(x.end) || sh.end.isEqual(x.end)))) {
      false
    } else {
      true
    }
  }

  property("Check if same resource is used in different roles ") = {
    forAll(genResources) { res: Resources =>
      forAll(genVivasResources(res)) { vs: List[Try[Viva]] =>
        vs.filter(_.isSuccess).map(_.get).forall(v =>{
        val coAdvisersViva: List[Resource] =v.coAdvisers
          !(v.president.id.s == v.adviser.id.s) ||
            checkInstance(coAdvisersViva).teachers.teacherList.exists(t => (t.id.s == v.president.id.s))||
            checkInstance(coAdvisersViva).teachers.teacherList.exists(t =>(t.id.s == v.adviser.id.s)) ||
            v.supervisors.externalList.exists(e => checkInstance(coAdvisersViva).externals.externalList.exists(
              f => f.id.s == e.id.s))

        }
        )
      }
    }
  }

  property("Compare nº Vivas with nº Vivas Scheduled") = {
    forAll(genResources) { res: Resources =>
      forAll(genVivasResources(res)) { vs: List[Try[Viva]] =>
        val fVivas = vs.filter(_.isSuccess).map(_.get)
        forAll(genAgendaDuration(res, Vivas(fVivas))) { ag: Try[Agenda] =>
          ag match {
            case Success(agenda)=>
              val sch: Try[CompleteSchedule] = Functions(agenda)
              sch match {
                case Success(cs) => cs.scheduledVivaList.size == fVivas.size
                case _=> true
              }
            case _=>true
          }
        }
      }
    }
  }

  property("Compare scheduled vivas with resources availabilities") = {
    forAll(genResources) { res: Resources =>
      forAll(genVivasResources(res)) { vs: List[Try[Viva]] =>
        val fVivas: List[Viva] = vs.filter(_.isSuccess).map(_.get)
        forAll(genAgendaDuration(res, Vivas(fVivas))) { ag: Try[Agenda] =>
          ag match {
            case Success(agenda) =>
              Functions(agenda) match {
                case Success(cs)=>
                  cs.scheduledVivaList.forall(sv =>
                    compareScheduleWithAvail(sv, fVivas.find(p => (p.title.s == sv.viva.title.s)).get)
                    //compareScheduleWithAvl(sv)
                  )
                case _=>true
              }
            case _=>true
          }
        }
      }
    }
  }

  property("Check if resources are being updated") = {
    forAll(genResources) { res: Resources =>
      forAll(genVivasResources(res)) { vs: List[Try[Viva]] =>
        val fVivas = vs.filter(_.isSuccess).map(_.get)
        forAll(genAgendaDuration(res, Vivas(fVivas))) { ag: Try[Agenda] =>
          ag match {
            case Success(agenda)=> Functions(agenda) match {
                case Success(cs)=> cs.scheduledVivaList.forall(sv =>
                  checkIfResAreUpdated(sv, sv.viva)
                )

                case _=>true
              }
            case _=>true
          }
        }
      }
    }
  }



  def oneResIsOverlappedIn2SV(completeSchedule: CompleteSchedule): Boolean = {

    val pairsList: List[(ScheduledViva, ScheduledViva)] = completeSchedule.scheduledVivaList.combinations(2).map { case List(a, b) => (a, b) }.toList

    @tailrec
    def getOverlappedRes(pairs: List[(ScheduledViva, ScheduledViva)]): Boolean = pairs match {
      case hp :: tp =>
        val resource1 = getResourcesFromSV(hp._1)
        val resource2 = getResourcesFromSV(hp._2)
        if ((resource1.externals.externalList.exists(ep1 => resource2.externals.externalList.exists(ep2 => ep2.id == ep1.id))
          || resource1.teachers.teacherList.exists(t1 => resource2.teachers.teacherList.exists(t2 => t2.id == t1.id))) &&
          (hp._1.start.isBefore(hp._2.end) && hp._1.end.isAfter(hp._2.start)))
          false
        else {
          getOverlappedRes(tp)
        }
      case _ => true
    }

    getOverlappedRes(pairsList)

  }


  property("One resource cannot be overlapped in two scheduled viva") = {
    forAll(genResources) {
      resources =>
        forAll(genVivasResources(resources)) {
          vivas: List[Try[Viva]] =>
            val fVivas = vivas.filter(_.isSuccess).map(_.getOrElse(???))
            forAll(genAgendaDuration(resources, Vivas(fVivas))) {
              ag: Try[Agenda] =>
                  ag match {
                    case Success(agenda)=>
                      val cs: Try[CompleteSchedule] =Functions(agenda)
                      cs match {
                        case Success(schedule)=>
                          if(schedule.scheduledVivaList.size>=2) {
                             val check= oneResIsOverlappedIn2SV(schedule)
                             check
                            }
                          else true
                        case _=>true
                      }
                    case _=>true
                  }
            }
        }
    }
  }


  property("Vivas must be scheduled in a First Come First Served manner") = {
    forAll(genResources) { res: Resources =>
      forAll(genVivasResources(res)) { vs: List[Try[Viva]] =>
        val fVivas = vs.filter(_.isSuccess).map(_.getOrElse(???))
        forAll(genAgendaDuration(res, Vivas(fVivas))) { ag: Try[Agenda] =>
          ag match {
            case Success(agenda)=>
              val vivas = agenda.vivas.listVivas
              Functions(agenda) match {
                case Success(schedule)=>
                  val pairs=vivas.zip(schedule.scheduledVivaList)
                  pairs.forall(p => p._1.title == p._2.viva.title)
                case _=>true
              }
            case _=>true
          }
        }
      }
    }
  }

  def checkTimeOrder(xs: List[ScheduledViva]): Boolean = {
    @tailrec
    def timeOrder(m: ScheduledViva, ys: List[ScheduledViva]): Boolean = ys match {
      case x :: t =>
        if (m.start.isBefore(x.start))
          timeOrder(x, t)
        else false
      case Nil => true
    }

    xs match {
      case head :: tail => timeOrder(head, tail)
      case _ => true
    }
  }

  property("Dependent Vivas are scheduled by time order") = {
    forAll(genResources) { res: Resources =>
      forAll(genVivasResources(res)) { vs: List[Try[Viva]] =>
        val fVivas = vs.filter(_.isSuccess).map(_.getOrElse(???))
        forAll(genAgendaDuration(res, Vivas(fVivas))) { ag: Try[Agenda] =>
          ag match {
            case Success(agenda)=>
              Functions(agenda) match {
                case Success(cs)=> if(vs.size==fVivas.size) checkTimeOrder(cs.scheduledVivaList)
                            else true
                case _=> true
              }
            case _=>true
          }}}}}


  property("Compare nº Vivas with nº Vivas Scheduled") = {
    forAll(genResources) { res: Resources =>
      forAll(genVivasResources(res)) { vs: List[Try[Viva]] =>
        val fVivas = vs.filter(_.isSuccess).map(_.get)
        forAll(genAgendaDuration(res, Vivas(fVivas))) { ag: Try[Agenda] =>
          ag match {
            case Success(agenda)=>
              val sch: Try[CompleteSchedule] = FunctionsMs03(agenda)
              sch match {
                case Success(cs) => cs.scheduledVivaList.size == fVivas.size
                case _=> true
              }
            case _=>true
          }
        }
      }
    }
  }




}
