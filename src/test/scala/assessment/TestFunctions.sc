import java.time.{LocalDateTime, LocalTime}
import java.time.temporal.ChronoUnit

import domain.schedule.{Agenda, Availability, ExternalPerson, Externals, NonEmptyString, Resources, Teacher, Teachers, Viva, Vivas}
import org.scalacheck.Gen

import scala.util.Try


 object Geradores{


 }
val genTryNonEmptyString: Gen[Try[NonEmptyString]]=for {
  s<-Gen.listOfN(10,Gen.alphaChar)
} yield NonEmptyString.from(s.mkString(""), "element")

genTryNonEmptyString.sample

val genNonEmptyString: Gen[NonEmptyString] =for{
  s<-Gen.listOfN(10,Gen.alphaChar)
  if(!s.mkString("").isEmpty)
}yield NonEmptyString.from(s.mkString(""),"").getOrElse(???)


def convertListToDiff(start:LocalDateTime,ld:List[Int]): List[LocalDateTime] ={
  ld.foldLeft[(LocalDateTime,List[LocalDateTime])](start,Nil)
  {
    case ((i: LocalDateTime,li: List[LocalDateTime]),d: Int)=>
      val ni: LocalDateTime =i.plus(d,ChronoUnit.MINUTES)
      (ni,li:+ni)
  }._2
}

def convertDiffToList(start:LocalDateTime,ld:List[Int]): List[LocalDateTime] ={
  @scala.annotation.tailrec
  def convIntsToDates(i:LocalDateTime, ld:List[Int], li:List[LocalDateTime]):List[LocalDateTime]=
    ld match {
    case d::td=>{
      val ni: LocalDateTime =i.plus(d,ChronoUnit.MINUTES)
      convIntsToDates(ni,td,li:+ni)
    }
    case _=>li
  }
  convIntsToDates(start,ld,List())

}




def genInstants(start:LocalDateTime)=for{
  n<-Gen.chooseNum(1,9)
  ld<-Gen.listOfN(n,Gen.chooseNum(10,60))
  }yield convertDiffToList(start,0::ld)


genInstants(LocalDateTime.of(2020,1,2,9,0)).sample

def genInstants1hTo2H(start:LocalDateTime)=for{
  n<-Gen.chooseNum(0,9)
  ld<-Gen.listOfN(n,Gen.chooseNum(60,120))
  }yield convertDiffToList(start,0::ld)

genInstants1hTo2H(LocalDateTime.of(2020,1,2,9,0)).sample

def genInstantsMore2H(start:LocalDateTime)=for{
  n<-Gen.chooseNum(0,9)
  ld<-Gen.listOfN(n,Gen.chooseNum(60,360))
}yield convertDiffToList(start,0::ld)


genInstantsMore2H(LocalDateTime.of(2020,1,2,9,0)).sample

val g: List[LocalDateTime] =genInstants(LocalDateTime.of(2020,1,2,9,0)).sample.get

val genAnInstant: Gen[LocalDateTime] =for{
  instant<-Gen.oneOf(genInstants(LocalDateTime.of(2020,1,2,9,0)).sample.get)
}yield instant

val gerarInstante=for{
  instants <-genInstants(LocalDateTime.of(2020,1,2,9,0))
  instant<-Gen.oneOf(instants)
}yield instant


genAnInstant.sample

val genLocalDataTime=for{
  year<-Gen.chooseNum(2020,2020)
  month<-Gen.chooseNum(1,12)
  day<-Gen.chooseNum(1,28)
  hour<-Gen.chooseNum(0,23)
  min<-Gen.chooseNum(1,59)
}yield LocalDateTime.of(year,month,day,hour,min)

genLocalDataTime.sample

def genAvailability: Gen[Availability] =for{
  start<-genAnInstant
  end<-genAnInstant
  preference<-Gen.size
}yield Availability(start,end,preference)

genAvailability.sample

  def genTeacher=for{
    id<-genNonEmptyString
    name<-genNonEmptyString
    n<-Gen.chooseNum(0,9)
    la<-Gen.listOfN(n,genAvailability)
  }yield Teacher(id,name,la)

  genTeacher.sample

  def genExternal=for{
    id<-genNonEmptyString
    name<-genNonEmptyString
    n<-Gen.chooseNum(0,9)
    la<-Gen.listOfN(n,genAvailability)
  }yield ExternalPerson(id,name,la)

  genExternal.sample






def genTeachers=for{
  n<-Gen.chooseNum(1,9)
  lt<-Gen.listOfN(n,genTeacher)
}yield Teachers(lt)

genTeachers.sample

def genExternals=for{
  n<-Gen.chooseNum(1,9)
  lt<-Gen.listOfN(n,genExternal)
}yield Externals(lt)

genExternals.sample



@scala.annotation.tailrec
def genCodAdviserT(tRes: Teachers, uRes: Teachers, result: List[Teacher], index: Int): Teachers = {
  if (index != result.size) {
    tRes.teacherList match {
      case Nil => Teachers(result)
      case h :: tail => if (!uRes.teacherList.exists(t => t.id.s != h.id.s)) {
        genCodAdviserT(Teachers(tail), uRes, h :: result, index + 1)
      } else {
        genCodAdviserT(Teachers(tail), uRes, result, index + 1)
      }
    }
  } else {
    Teachers(result)
  }
}

@scala.annotation.tailrec
def genCodAdviserE(tRes: Externals, uRes: Externals, result: List[ExternalPerson], index: Int): Externals = {
  if (index != result.size) {
    tRes.externalList match {
      case Nil => Externals(result)
      case h :: tail => if (!uRes.externalList.exists(t => t.id.s != h.id.s)) {
        genCodAdviserE(Externals(tail), uRes, h :: result, index + 1)
      } else {
        genCodAdviserE(Externals(tail), uRes, result, index + 1)
      }
    }
  } else {
    Externals(result)
  }
}

def genViva(res: Resources) = for {
  id <- genNonEmptyString
  name <- genNonEmptyString

  president <- Gen.oneOf(res.teachers.teacherList)
  adviser <- Gen.oneOf(res.teachers.teacherList.filter(p => p.id != president.id))

  nCodAT <- Gen.chooseNum(0, res.teachers.teacherList.size)
  codAdviserT = genCodAdviserT(res.teachers, Teachers(president :: adviser :: List()), List(), nCodAT)

  nCodAE <- Gen.chooseNum(0, res.externals.externalList.size)
  codAdviserE = genCodAdviserE(res.externals, Externals(List()), List(), nCodAE)

  nSuper <- Gen.chooseNum(0, res.externals.externalList.size)
  superVisor = genCodAdviserE(res.externals, codAdviserE, List(), nSuper)
} yield Viva(id, name, president, adviser, Resources(codAdviserT, codAdviserE), superVisor)

def genVivas(res: Resources) = for {
  n <- Gen.chooseNum(1, 9)
  lt <- Gen.listOfN(n, genViva(res))
} yield Vivas(lt)

def genTeachersViva = for {
  n <- Gen.chooseNum(0, 9)
  lt <- Gen.listOfN(n, genTeacher)
} yield Teachers(lt)

def genExternalsViva = for {
  n <- Gen.chooseNum(0, 9)
  lt <- Gen.listOfN(n, genExternal)
} yield Externals(lt)

def genVivaRand = for {
  id <- genNonEmptyString
  name <- genNonEmptyString

  president <- genTeachersViva.map(t => t.teacherList.head)
  adviser <- genTeachersViva.map(t => t.teacherList.head)

  codAdviserT <- genTeachersViva
  codAdviserE <- genExternalsViva

  superVisor <- genExternalsViva
} yield Viva(id, name, president, adviser, Resources(codAdviserT, codAdviserE), superVisor)

def genVivasRand = for {
  n <- Gen.chooseNum(1, 9)
  lt <- Gen.listOfN(n, genVivaRand)
} yield Vivas(lt)

def genAgenda = for {
  resT <- genTeachers
  resE <- genExternals
  vivas <- genVivasRand
  hour <- Gen.chooseNum(0, 5)
  minute <- Gen.chooseNum(0, 59)
} yield Agenda(vivas, Resources(resT, resE), LocalTime.of(hour, minute))

1==2
