package assessment
import java.time.{LocalDateTime, LocalTime}

import assessment.MS03Algorithm._
import domain.schedule._
import org.scalatest.funsuite.AnyFunSuite

class MS03AlgorithmTest extends AnyFunSuite {

  val disp9 = Availability(LocalDateTime.parse("2020-05-30T09:30:00"), LocalDateTime.parse("2020-05-30T12:30:00"), 5)

  val disp1 = Availability(LocalDateTime.parse("2020-05-30T09:30:00"), LocalDateTime.parse("2020-05-30T12:30:00"), 5)
  val disp2 = Availability(LocalDateTime.parse("2020-05-30T13:30:00"), LocalDateTime.parse("2020-05-30T16:30:00"), 3)

  val disp3 = Availability(LocalDateTime.parse("2020-05-30T10:30:00"), LocalDateTime.parse("2020-05-30T11:30:00"), 5)
  val disp4 = Availability(LocalDateTime.parse("2020-05-30T14:30:00"), LocalDateTime.parse("2020-05-30T17:00:00"), 5)

  val disp5 = Availability(LocalDateTime.parse("2020-05-30T10:00:00"), LocalDateTime.parse("2020-05-30T13:30:00"), 2)
  val disp6 = Availability(LocalDateTime.parse("2020-05-30T15:30:00"), LocalDateTime.parse("2020-05-30T18:00:00"), 5)

  val disp7 = Availability(LocalDateTime.parse("2020-05-30T08:08:00"), LocalDateTime.parse("2020-05-30T10:38:00"), 4)
  val disp8 = Availability(LocalDateTime.parse("2020-05-30T15:43:00"), LocalDateTime.parse("2020-05-30T17:28:00"), 5)

  val t1: Teacher =Teacher(NonEmptyString.from("T001","id").get,
    NonEmptyString.from("Teacher 001","name").get, List(disp1,disp2))

  val t2: Teacher = Teacher(NonEmptyString.from("T002", element = "").get,
    NonEmptyString.from("Teacher 002", element = "").get, List(disp3, disp4))

  val t3: Teacher = Teacher(NonEmptyString.from("T003", element = "").get,
    NonEmptyString.from("Teacher 003", element = "").get, List(disp1, disp3))

  val t4: Teacher = Teacher(NonEmptyString.from("T004", element = "").get,
    NonEmptyString.from("Teacher 004", element = "").get, List(disp1, disp3))

  val t5: Teacher = Teacher(NonEmptyString.from("T005", element = "").get,
    NonEmptyString.from("Teacher 005", element = "").get, List(disp1, disp3))

  val t6: Teacher = Teacher(NonEmptyString.from("T006", element = "").get,
    NonEmptyString.from("Teacher 006", element = "").get, List(disp5, disp6))

  val e1: ExternalPerson = ExternalPerson(NonEmptyString.from("E001", element = "").get,
    NonEmptyString.from("External 001", element = "").get,
    List(disp5, disp6))

  val e2: ExternalPerson = ExternalPerson(NonEmptyString.from("E002", element = "").get,
    NonEmptyString.from("External 002", element = "").get, List(disp2, disp4))

  val e3: ExternalPerson = ExternalPerson(NonEmptyString.from("E003", element = "").get,
    NonEmptyString.from("External 003", element = "").get, List(disp2, disp4))

  val e4: ExternalPerson = ExternalPerson(NonEmptyString.from("E003", element = "").get,
    NonEmptyString.from("External 003", element = "").get, List(disp2, disp4))



  val v1=Viva(NonEmptyString.from("V001", element = "").get,NonEmptyString.from("Student 001", element = "").get,
    t1,t2,List(),Externals(List(e1)))

  val v2=Viva(NonEmptyString.from("V002", element = "").get,NonEmptyString.from("Student 002", element = "").get,
    t2,t1,List(),Externals(List()))

  val v3=Viva(NonEmptyString.from("V003", element = "").get,NonEmptyString.from("Student 003", element = "").get,
    t2,t4,List(),Externals(List()))

  val v4=Viva(NonEmptyString.from("V004", element = "").get,NonEmptyString.from("Student 004", element = "").get,
    t5,t6,List(),Externals(List()))

  val v5=Viva(NonEmptyString.from("V005", element = "").get,NonEmptyString.from("Student 005", element = "").get,
    t6,t5,List(),Externals(List(e2)))




  test("test getInterval") {

    assert(getInterval(LocalDateTime.parse("2020-05-30T10:30:00"), LocalDateTime.parse("2020-05-30T11:30:00"))
      === LocalTime.parse("01:00:00"))

    assert(getInterval(LocalDateTime.parse("2020-05-30T10:30:00"), LocalDateTime.parse("2020-05-30T10:30:00"))
      === LocalTime.parse("00:00:00"))

    assert(getInterval(LocalDateTime.parse("2020-05-30T11:30:00"), LocalDateTime.parse("2020-05-30T10:30:00"))
      === LocalTime.parse("01:00:00"))

    assert(getInterval(LocalDateTime.parse("2020-05-30T21:30:00"), LocalDateTime.parse("2020-05-31T09:30:00"))
      === LocalTime.parse("12:00:00"))
  }

  test("test overlapDateRange not found") {
    assert(overlapDateRange(List(disp5, disp3, disp2), LocalTime.parse("01:00:00"))
      === Availability(LocalDateTime.MIN, LocalDateTime.MIN, 0))
  }

  test("test overlapDateRange found") {
    assert(overlapDateRange(List(disp5, disp3, disp1), LocalTime.parse("01:00:00"))
      === Availability(LocalDateTime.parse("2020-05-30T10:30:00"), LocalDateTime.parse("2020-05-30T11:30:00"), 12))

    assert(overlapDateRange(List(disp5), LocalTime.parse("01:00:00"))
      === Availability(LocalDateTime.parse("2020-05-30T10:00:00"), LocalDateTime.parse("2020-05-30T13:30:00"), 2))
  }

  test("test empty overlapDateRange") {
    assert(overlapDateRange(List(), LocalTime.parse("01:00:00"))
      === Availability(LocalDateTime.MIN, LocalDateTime.MIN, 0))
  }

  test("test findFirstOverlap found") {
    assert(findFirstOverlap(List(List(disp5, disp3, disp1),List(disp6,disp4,disp2)),LocalTime.parse("01:00:00")) ===
      Availability(LocalDateTime.parse("2020-05-30T10:30:00"), LocalDateTime.parse("2020-05-30T11:30:00"), 12))
  }

  test("test findFirstOverlap not found") {
    assert(findFirstOverlap(List(List(disp5, disp3, disp2),List(disp5, disp3, disp2)),LocalTime.parse("01:00:00")) ===
      Availability(LocalDateTime.MIN, LocalDateTime.MIN, 0))
  }

  test("test empty checkNonOverlap") {
    assert(checkNonOverlap(List()))
  }

  test("test checkNonOverlap true") {
    assert(checkNonOverlap(List(disp1,disp2)))
  }

  test("test checkNonOverlap false") {
    assert(!checkNonOverlap(List(disp1,disp2,disp3)))
  }

  test("test checkInstance teacher") {
    assert(checkInstance(
      List(Teacher(NonEmptyString.from("T1","id").get,NonEmptyString.from("Teacher 1","name").get,
        List(disp1,disp2))))===Resources(Teachers(List(Teacher(NonEmptyString.from("T1","id").get,
      NonEmptyString.from("Teacher 1","name").get, List(disp1,disp2)))),Externals(List())))
  }

  test("test checkInstance external") {
    assert(checkInstance(
      List(ExternalPerson(NonEmptyString.from("E1","id").get,
        NonEmptyString.from("External 1","name").get,
        List(disp1,disp2))))===Resources(Teachers(List()),Externals(
      List(ExternalPerson(NonEmptyString.from("E1","id").get,
      NonEmptyString.from("External 1","name").get,
      List(disp1,disp2))))))
  }

  test("test checkInstance teacher and external") {
    assert(checkInstance(
      List(Teacher(NonEmptyString.from("T1","id").get,NonEmptyString.from("Teacher 1","name").get,
        List(disp1,disp2)),ExternalPerson(NonEmptyString.from("E1","id").get,
        NonEmptyString.from("External 1","name").get,
        List(disp1,disp2))))===
      Resources(Teachers(List(Teacher(NonEmptyString.from("T1","id").get,
      NonEmptyString.from("Teacher 1","name").get,
      List(disp1,disp2)))),Externals(
      List(ExternalPerson(NonEmptyString.from("E1","id").get,
        NonEmptyString.from("External 1","name").get,
        List(disp1,disp2))))))
  }

  test("getResourcesFromViva") {
    assert(getResourcesFromViva(v1)===Resources(Teachers(List(t1,t2)),Externals(List(e1))))
  }

  /*test("getIndependentVivas") {
    assert(getIndependentVivas(List(v1,v2,v3,v4))===List(v4))
  }

  test("getDependentVivas") {
    assert(getDependentVivas(List(v1,v2,v3,v4))===List(v1,v2,v3))
  }*/

  test(" test getDependentAndIndependentVivas") {
    assert(getIndependentAndDependentVivas(List(v1,v2,v3,v4))===List(List(v4),List(v1,v2,v3)))
  }


  test(" test getAllDependenciesGroups 1") {
    assert(getAllDependenciesGroups(List(v1,v2,v3,v4,v5))===List(List(v4,v5),List(v1,v2,v3)))
  }

  test(" test getAllDependenciesGroups 2") {
    assert(getAllDependenciesGroups(List(v4,v1,v3,v2,v5))===List(List(v4,v5),List(v1,v3,v2)))
  }





}

