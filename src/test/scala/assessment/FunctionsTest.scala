package assessment

import java.time.{LocalDateTime, LocalTime}

import assessment.Functions._
import domain.schedule._

class FunctionsTest extends org.scalatest.funsuite.AnyFunSuite {
  val disp1 = Availability(LocalDateTime.parse("2020-05-30T09:30:00"), LocalDateTime.parse("2020-05-30T12:30:00"), 5)
  val disp2 = Availability(LocalDateTime.parse("2020-05-30T13:30:00"), LocalDateTime.parse("2020-05-30T16:30:00"), 3)

  val disp3 = Availability(LocalDateTime.parse("2020-05-30T10:30:00"), LocalDateTime.parse("2020-05-30T11:30:00"), 5)
  val disp4 = Availability(LocalDateTime.parse("2020-05-30T14:30:00"), LocalDateTime.parse("2020-05-30T17:00:00"), 5)

  val disp5 = Availability(LocalDateTime.parse("2020-05-30T10:00:00"), LocalDateTime.parse("2020-05-30T13:30:00"), 2)
  val disp6 = Availability(LocalDateTime.parse("2020-05-30T15:30:00"), LocalDateTime.parse("2020-05-30T18:00:00"), 5)

  test("test getInterval") {

    assert(getInterval(LocalDateTime.parse("2020-05-30T10:30:00"), LocalDateTime.parse("2020-05-30T11:30:00"))
      === LocalTime.parse("01:00:00"))

    assert(getInterval(LocalDateTime.parse("2020-05-30T10:30:00"), LocalDateTime.parse("2020-05-30T10:30:00"))
      === LocalTime.parse("00:00:00"))

    assert(getInterval(LocalDateTime.parse("2020-05-30T11:30:00"), LocalDateTime.parse("2020-05-30T10:30:00"))
      === LocalTime.parse("01:00:00"))
  }

  test("test overlapDateRange not found") {
    assert(overlapDateRange(List(disp5, disp3, disp2), LocalTime.parse("01:00:00"))
      === Availability(LocalDateTime.MIN, LocalDateTime.MIN, 0))
  }

  test("test overlapDateRange found") {
    assert(overlapDateRange(List(disp5, disp3, disp1), LocalTime.parse("01:00:00"))
      === Availability(LocalDateTime.parse("2020-05-30T10:30:00"), LocalDateTime.parse("2020-05-30T11:30:00"), 12))

  }

  test("test combinationsAvailabilities") {
    assert(combinationsAvailabilities(List(List(disp1, disp2), List(disp3, disp4), List(disp5, disp6))) ===
      List(List(disp1, disp3, disp5), List(disp2, disp3, disp5), List(disp1, disp4, disp5), List(disp1, disp3, disp6),
        List(disp2, disp4, disp5), List(disp2, disp3, disp6), List(disp1, disp4, disp6), List(disp2, disp4, disp6)))
  }

  test("test createCompleteSchedule") {

    val t1 = Teacher(NonEmptyString.from("T001", element = "").get, NonEmptyString.from("Teacher 001", element = "").get,
      List(disp1, disp2))
    val t2 = Teacher(NonEmptyString.from("T002", element = "").get, NonEmptyString.from("Teacher 002", element = "").get,
      List(disp3, disp4))
    val e1 = ExternalPerson(NonEmptyString.from("E001", element = "").get, NonEmptyString.from("External 001", element = "").get,
      List(disp5, disp6))

    val res = Resources(Teachers(List(t1, t2)), Externals(List(e1)))

    val viva = Viva(NonEmptyString.from("Title 1", element = "").get, NonEmptyString.from("Student 001", element = "").get,
      t1, t2, List(), Externals(List(e1)))

    val agenda = Agenda(Vivas(List(viva)), res, LocalTime.parse("01:00:00"))

    assert(createCompleteSchedule(agenda) === CompleteSchedule(List(ScheduledViva(LocalDateTime.parse("2020-05-30T10:30:00"),
                                              LocalDateTime.parse("2020-05-30T11:30:00"), viva, 12))))
  }
}
