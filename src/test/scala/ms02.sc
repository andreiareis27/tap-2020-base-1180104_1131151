import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

import domain.schedule._
import org.scalacheck.Gen

val genNonEmptyString: Gen[NonEmptyString] = for {
  s <- Gen.listOfN(10, Gen.alphaChar)
  if !s.mkString("").isEmpty
} yield NonEmptyString.from(s.mkString(""), "").getOrElse(???)

def convertDiffToInst(start: LocalDateTime, ld: List[Int]): List[LocalDateTime] =
  ld.foldLeft[(LocalDateTime, List[LocalDateTime])](start, Nil) { case ((i, li), d) =>
    val ni = i.plus(d, ChronoUnit.MINUTES)
    (ni, li :+ ni)
  }._2

def genInstants(start: LocalDateTime): Gen[List[LocalDateTime]] = for {
  n <- Gen.chooseNum(minT = 0, maxT = 9)
  ld <- Gen.listOfN(n, Gen.chooseNum(minT = 10, maxT = 60))
} yield convertDiffToInst(start, 0 :: ld)

def genAvailability: Gen[Availability] = for {
  start <- Gen.oneOf(genInstants(LocalDateTime.now()).sample.get)
  end <- Gen.oneOf(genInstants(start).sample.get)
  pref <- Gen.choose[Int](1, 5)
} yield Availability(start, end, pref)

def genTeacher: Gen[Teacher] = for {
  id <- genNonEmptyString
  name <- genNonEmptyString
  n <- Gen.chooseNum(0, 9)
  la <- Gen.listOfN(n, genAvailability)
} yield Teacher(id, name, la)

def genExternal = for {
  id <- genNonEmptyString
  name <- genNonEmptyString
  n <- Gen.chooseNum(0, 9)
  la <- Gen.listOfN(n, genAvailability)
} yield ExternalPerson(id, name, la)

def genTeachers = for {
  n <- Gen.chooseNum(1, 9)
  lt <- Gen.listOfN(n, genTeacher)
} yield Teachers(lt)

def genExternals = for {
  n <- Gen.chooseNum(1, 9)
  lt <- Gen.listOfN(n, genExternal)
} yield Externals(lt)

@scala.annotation.tailrec
def genCodAdviserT(tRes: Teachers, uRes: Teachers, result: List[Teacher], index: Int): Teachers = {
  if (index != result.size) {
    tRes.teacherList match {
      case Nil => Teachers(result)
      case h :: tail => if (!uRes.teacherList.exists(t => t.id.s != h.id.s)) {
        genCodAdviserT(Teachers(tail), uRes, h :: result, index + 1)
      } else {
        genCodAdviserT(Teachers(tail), uRes, result, index + 1)
      }
    }
  } else {
    Teachers(result)
  }
}

@scala.annotation.tailrec
def genCodAdviserE(tRes: Externals, uRes: Externals, result: List[ExternalPerson], index: Int): Externals = {
  if (index != result.size) {
    tRes.externalList match {
      case Nil => Externals(result)
      case h :: tail => if (!uRes.externalList.exists(t => t.id.s != h.id.s)) {
        genCodAdviserE(Externals(tail), uRes, h :: result, index + 1)
      } else {
        genCodAdviserE(Externals(tail), uRes, result, index + 1)
      }
    }
  } else {
    Externals(result)
  }
}

/*
def genViva(res: Resources) = for {
  id <- genNonEmptyString
  name <- genNonEmptyString

  president <- Gen.oneOf(res.teachers.teacherList)
  adviser <- Gen.oneOf(res.teachers.teacherList.filter(p => p.id != president.id))

  nCodAT <- Gen.chooseNum(0, res.teachers.teacherList.size)
  codAdviserT = genCodAdviserT(res.teachers, Teachers(president :: adviser :: List()), List(), nCodAT)

  nCodAE <- Gen.chooseNum(0, res.externals.externalList.size)
  codAdviserE = genCodAdviserE(res.externals, Externals(List()), List(), nCodAE)

  nSuper <- Gen.chooseNum(0, res.externals.externalList.size)
  superVisor = genCodAdviserE(res.externals, codAdviserE, List(), nSuper)
} yield Viva(id, name, president, adviser, Resources(codAdviserT, codAdviserE), superVisor)

def genVivas(res: Resources) = for {
  n <- Gen.chooseNum(1, 9)
  lt <- Gen.listOfN(n, genViva(res))
} yield Vivas(lt)

def genTeachersViva = for {
  n <- Gen.chooseNum(0, 9)
  lt <- Gen.listOfN(n, genTeacher)
} yield Teachers(lt)

def genExternalsViva = for {
  n <- Gen.chooseNum(0, 9)
  lt <- Gen.listOfN(n, genExternal)
} yield Externals(lt)

def genVivaRand() = for {
  id <- genNonEmptyString
  name <- genNonEmptyString

  president <- genTeachersViva.map(t => t.teacherList.head)
  adviser <- genTeachersViva.map(t => t.teacherList.head)

  codAdviserT <- genTeachersViva
  codAdviserE <- genExternalsViva

  superVisor <- genExternalsViva
} yield Viva(id, name, president, adviser, Resources(codAdviserT, codAdviserE), superVisor)

def genVivasRand = for {
  n <- Gen.chooseNum(1, 9)
  lt <- Gen.listOfN(n, genVivaRand())
} yield Vivas(lt)

def genAgenda = for {
  resT <- genTeachers
  resE <- genExternals
  vivas <- genVivasRand
  hour <- Gen.chooseNum(0, 5)
  minute <- Gen.chooseNum(0, 59)
} yield Agenda(vivas, Resources(resT, resE), LocalTime.of(hour, minute))

genAgenda.sample*/
