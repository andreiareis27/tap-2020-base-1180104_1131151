package domain.schedule

import scala.util.{Failure, Success, Try}

sealed abstract case class NonEmptyString(s:String)
object NonEmptyString{
  def from(s:String, element: String):Try[NonEmptyString]=
    if(s!=null && !s.isEmpty) Success(new NonEmptyString(s) {})
    else
      Failure(new Exception(s"Empty string in $element"))
}
