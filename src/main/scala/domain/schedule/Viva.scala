package domain.schedule

import scala.util.{Failure, Success, Try}
import scala.xml.Node
import assessment.Functions.checkInstance

case class Viva(title: NonEmptyString, student: NonEmptyString, president: Teacher, adviser: Teacher,
                coAdvisers: List[Resource], supervisors: Externals)

object Viva {

  private def createMapTeacher(resources: Resources): Map[Try[NonEmptyString], Teacher] = {
    resources.teachers.teacherList.foldLeft[Map[Try[NonEmptyString], Teacher]](Map()) {
      case (m: Map[Try[NonEmptyString], Teacher], t: Teacher) => m + (Try(t.id) -> t)
    }
  }


  private def findExternal(n:Node,resources: Resources)={
    val d: Option[ExternalPerson] =resources.externals.externalList.find(e => e.id.s == (n \@ "id").toString)
    d match {
      case Some(ep)=>Success(ep)
      case None=>Failure(new Exception("id " +(n \@ "id")+ " does not exist in externals list"))
    }
  }

  private def findTeacher(n:Node,resources: Resources)={
    val d: Option[Teacher] =resources.teachers.teacherList.find(e => e.id.s == (n \@ "id").toString)
    d match {
      case Some(t)=>Success(t)
      case None=>Failure(new Exception("id "+(n \@ "id")+" does not exist in teachers list"))
    }
  }

  private def findResource(n:Node,resources: Resources):Try[Resource]={
    val t: Option[Teacher] =resources.teachers.teacherList.find(e => e.id.s == (n \@ "id").toString)
    val e: Option[ExternalPerson] =resources.externals.externalList.find(e => e.id.s == (n \@ "id").toString)
    t match {
      case Some(teacher)=>Success(teacher)
      case None=>e match {
        case Some(external)=> Success(external)
        case None=> Failure(new Exception("id "+(n \@ "id")+" does not exist in teachers list"))
      }
    }
  }

  private def lSupervisors(n: Node, resources: Resources): Try[List[ExternalPerson]] =
    (n \ "supervisor").foldLeft[Try[List[ExternalPerson]]](Success(Nil)) {
      case (ols, n) => for {
        lAvailable <- ols
        s<-findExternal(n,resources)
      } yield lAvailable :+ s
    }


  private def lCoAdvisers(n: Node, resources: Resources): Try[List[Resource]] =
    (n \ "coadviser").foldLeft[Try[List[Resource]]](Success(Nil)) {
      case (ols, n) => for {
        lResource <- ols
        r <- findResource(n,resources)
      } yield lResource :+ r
    }


  private def tryPresident(n:Node,mapTeacher: Map[Try[NonEmptyString], Teacher]): Try[Teacher] ={
    val pr = Try(mapTeacher(NonEmptyString.from(n \ "president" \@ "id", element = "president")))
    pr match {
      case Success(t)=>Success(t)
      case Failure(_)=>Failure(new Exception("Node president is empty/undefined in viva"))
    }
  }

  private def tryAdviser(n:Node,mapTeacher: Map[Try[NonEmptyString], Teacher]): Try[Teacher] ={

    val ad=Try(mapTeacher(NonEmptyString.from(n \ "adviser" \@ "id", element = "adviser")))
    ad match {
      case Success(t)=>Success(t)
      case Failure(_)=>Failure(new Exception("Node adviser is empty/undefined in viva"))
    }
  }

  private def fromXml(n: Node, resources: Resources): Try[Viva] = {
    val th: Try[NonEmptyString] = NonEmptyString.from(n \@ "title", element = "title")
    val st: Try[NonEmptyString] = NonEmptyString.from(n \@ "student", element = "student")
    val mapTeacher: Map[Try[NonEmptyString], Teacher] = createMapTeacher(resources)
    val pr: Try[Teacher] =tryPresident(n,mapTeacher)
    val ad: Try[Teacher] = tryAdviser(n,mapTeacher)
    val coA: Try[List[Resource]] =lCoAdvisers(n,resources)
    val sp = lSupervisors(n, resources)
    for {
      title: NonEmptyString <- th
      student: NonEmptyString <- st
      president: Teacher <- pr
      adviser: Teacher <- ad
      coAdvisers<-coA
      supervisors <- sp.map(Externals(_))
    } yield Viva(title, student, president, adviser, coAdvisers, supervisors)
  }


  def from(title: NonEmptyString, student: NonEmptyString, president: Teacher, adviser: Teacher,
           coAdvisers: List[Resource], supervisors: Externals): Try[Viva] = {

    if (president.id == adviser.id){
      val message = "Adviser is the same as the president in viva "+title.s
      Failure(new Exception(message))
    } else if (checkInstance(coAdvisers).teachers.teacherList.exists(t => t.id.s == president.id.s))
    {
      val message = "CodAdviser is the same as the president in viva "+title.s
      Failure(new Exception(message))
    } else if (checkInstance(coAdvisers).teachers.teacherList.map(t=>t.id).distinct.size!=
      checkInstance(coAdvisers).teachers.teacherList.size||
      checkInstance(coAdvisers).externals.externalList.map(e=>e.id).distinct.size!=
        checkInstance(coAdvisers).externals.externalList.size) {
      val message = "There are CodAdvisers with the same id in viva "+title.s
      Failure(new Exception(message))
    } else if (checkInstance(coAdvisers).teachers.teacherList.exists(t => t.id.s == adviser.id.s)) {
      val message = "CodAdviser is same as the adviser in viva "+title.s
      Failure(new Exception(message))
    } else if (supervisors.externalList.exists(
        e => checkInstance(coAdvisers).externals.externalList.exists(f => f.id.s == e.id.s))) {
      val message = "Supervisor is same as co-Adviser in viva "+title.s
      Failure(new Exception(message))
    } else if (supervisors.externalList.map(ep=>ep.id.s).distinct.size!=supervisors.externalList.size) {
      val message = "There are Supervisors with the same id in viva "+title.s
      Failure(new Exception(message))
    }
    else Success(Viva(title, student, president, adviser, coAdvisers, supervisors))
  }


  def from(n: Node, resources: Resources): Try[Viva] ={

    for{
      v<-fromXml(n,resources)
      tryV<-from(v.title,v.student,v.president,v.adviser,v.coAdvisers,v.supervisors)
    } yield tryV

  }
}