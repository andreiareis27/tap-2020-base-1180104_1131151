package domain.schedule

import scala.util.{Failure, Success, Try}
import scala.xml.Node

case class Resources(teachers: Teachers, externals: Externals)

object Resources {

  /*def from(n: Node): Try[Resources] = {
    for {
      nt <- (n \ "teachers").headOption.fold[Try[Node]](Failure(new Exception("node teachers does not exist!")))(Success(_))
      teachers <- Try(Teachers.from(nt))
      ne <- (n \ "externals").headOption.fold[Try[Node]](Failure(new Exception("node externals does not exist!")))(Success(_))
      externals <- Try(Externals.from(ne))
    } yield Resources.from(teachers, externals).get
  }

  def from(teachers: Try[Teachers], externals: Try[Externals]): Try[Resources] = {
    if (teachers.isFailure) {
      Failure(new Exception(teachers.failed.get.getMessage))
    } else if (externals.isFailure) {
      Failure(new Exception(externals.failed.get.getMessage))
    } else Success(Resources(teachers.get, externals.get))
  }*/

  private def fromXml(n: Node): Try[Resources] = {
    for {
      nt <- (n \ "teachers").headOption.fold[Try[Node]](Failure(new Exception("node teachers does not exist!")))(Success(_))
      teachers <- Teachers.from(nt)
      ne <- (n \ "externals").headOption.fold[Try[Node]](Failure(new Exception("node externals does not exist!")))(Success(_))
      externals <- Externals.from(ne)
    } yield Resources(teachers, externals)
  }


  /*def from(teachers: Teachers, externals: Externals): Try[Resources] = {
    if (teachers.teacherList.size!=teachers.teacherList.map(t=>t.id).distinct.size||
      externals.externalList.size!=externals.externalList.map(e=>e.id).distinct.size) {
      Failure(new Exception("Resources must be distinct"))
    }
     else Success(Resources(teachers, externals))
  }*/


  def from(n:Node): Try[Resources] ={

    val resources=for{
      res<-fromXml(n)
    }yield res

    resources
  }



}
