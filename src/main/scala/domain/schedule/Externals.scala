package domain.schedule

import scala.util.{Failure, Success, Try}
import scala.xml.Node

case class Externals(externalList: List[ExternalPerson])

object Externals {

  private def fromXml(n: Node): Try[Externals] = {
    val le: Try[List[ExternalPerson]] = (n \\ "external").foldLeft[Try[List[ExternalPerson]]](Success(Nil)) {
      case (olt, n) => for {
        le <- olt
        external <- ExternalPerson.from(n)
      } yield le :+ external
    }

    le.map(Externals(_))
  }

  def from(externalList: List[ExternalPerson]): Try[Externals] ={
    if(externalList.map(ep=>ep.id).distinct.size==externalList.size)
      Success(Externals(externalList))
    else Failure(new Exception("There are repeated externals in the Resources list"))
  }

  def from(n: Node): Try[Externals] = {
      val externals=for{
        e<-fromXml(n)
        tryE<-from(e.externalList)
      }yield tryE
      externals
  }
}