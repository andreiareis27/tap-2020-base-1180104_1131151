package domain.schedule

import java.time.LocalDateTime

import scala.xml.Elem

case class ScheduledViva (start : LocalDateTime, end : LocalDateTime, viva : Viva, preference : Int) {

}

object ScheduledViva {
  def apply(start : LocalDateTime, end : LocalDateTime, viva : Viva, preference : Int) : ScheduledViva =
    new ScheduledViva(start, end, viva, preference)





  def checkInstance(resourcesList:List[Resource]): Resources ={

    @scala.annotation.tailrec
    def instance(r:List[Resource], t:List[Teacher], e:List[ExternalPerson]):Resources={
      r match {
        case h::tail=> h match {
          case Teacher(id,name,availability)=>{
            instance(tail,t:+Teacher(id,name,availability),e)
          }
          case ExternalPerson(id,name,availability)=>{
            instance(tail,t,e:+ExternalPerson(id,name,availability))
          }
        }
        case  Nil=> Resources(Teachers(t),Externals(e))
      }
      //Resources(Teachers(t),Externals(e))
    }

    instance(resourcesList,List(),List())

  }





  def getDataWithSeconds(localDateTime: LocalDateTime ): String ={
    if(localDateTime.getSecond==0)
      localDateTime.toString+":00"
    else
      localDateTime.toString
  }

  def tCoAdviserToXML(teacher: Teacher): Elem ={
      <coadviser name={teacher.name.s}/>
  }

  def epCoAdviserToXMl(externalPerson: ExternalPerson): Elem ={
      <coadviser name={externalPerson.name.s}/>
  }

  def supervisorToXMl(externalPerson: ExternalPerson): Elem ={
      <supervisor name={externalPerson.name.s}/>
  }

  def toXML(scheduledViva: ScheduledViva): Elem= {

    val coAd: Resources =checkInstance(scheduledViva.viva.coAdvisers)
    <viva student={scheduledViva.viva.student.s} title={scheduledViva.viva.title.s}
          start={getDataWithSeconds(scheduledViva.start)}
          end={getDataWithSeconds(scheduledViva.end)}
          preference={scheduledViva.preference.toString}>
      <president name={scheduledViva.viva.president.name.s}/>
      <adviser name={scheduledViva.viva.adviser.name.s}/>
      {
        if (coAd.teachers.teacherList.nonEmpty) {
          coAd.teachers.teacherList.map(t => tCoAdviserToXML(t))
        }
      }
      {
      if(coAd.externals.externalList.nonEmpty){
        coAd.externals.externalList.map(ex=>epCoAdviserToXMl(ex))
        }
      }
      {
        if(scheduledViva.viva.supervisors.externalList.nonEmpty){
          scheduledViva.viva.supervisors.externalList.map(ep=>supervisorToXMl(ep))
        }
      }

    </viva>
  }



}

