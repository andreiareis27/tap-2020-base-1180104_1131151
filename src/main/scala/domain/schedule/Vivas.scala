package domain.schedule
import scala.util.{Failure, Success, Try}
import scala.xml.Node

case class Vivas (listVivas: List[Viva])

object Vivas {

  def from(vivaList: List[Viva]): Try[Vivas] ={
    if(vivaList.map(t=>t.title).distinct.size==vivaList.size)
      Success(Vivas(vivaList))
    else Failure(new Exception("There are vivas with the same title in the Vivas list"))
  }

  private def fromXml(n : Node, resources : Resources) : Try[Vivas] = {
    val lv: Try[List[Viva]] = (n \\ "viva").foldLeft[Try[List[Viva]]](Success(Nil)) {
      case (olt, n) => for {
        lv <- olt
        viva <- Viva.from(n, resources)
      } yield lv :+ viva
    }

    lv.map(Vivas(_))
  }

  def from(n : Node, resources : Resources): Try[Vivas] = {
    val vivas=for{
      v<-fromXml(n,resources)
      tryV <-from(v.listVivas)
    } yield tryV
    vivas
  }

}