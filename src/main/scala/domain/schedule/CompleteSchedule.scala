package domain.schedule

import scala.xml.Elem

case class CompleteSchedule (scheduledVivaList: List[ScheduledViva]) {
}

object CompleteSchedule {
  def apply(scheduledVivaList: List[ScheduledViva]) : CompleteSchedule = new CompleteSchedule(scheduledVivaList)

  def toXML(completeSchedule: CompleteSchedule): Elem=
    <schedule
    xsi:noNamespaceSchemaLocation="../../schedule.xsd"
    totalPreference={completeSchedule.scheduledVivaList.map(sv=>sv.preference).sum.toString}
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      {completeSchedule.scheduledVivaList.map(sv=>ScheduledViva.toXML(sv))}
    </schedule>

}
