package domain.schedule

import scala.util.{Failure, Success, Try}
import scala.xml.Node
import assessment.MS03Algorithm.checkNonOverlap

case class ExternalPerson (id : NonEmptyString, name : NonEmptyString, availability : List[Availability]) extends Resource

object ExternalPerson {


  private def lAvailabilities(n: Node): Try[List[Availability]] =
    (n \\ "availability").foldLeft[Try[List[Availability]]](Success(Nil)) {
      case (ols, n) => for {
        lAvailable <- ols
        p <- Availability.from(n)
      } yield lAvailable :+ p
    }

  private def fromXml(n: Node): Try[ExternalPerson] = {
    for {
      id <- NonEmptyString.from(n \@ "id", element = "external id")
      name <- NonEmptyString.from(n \@ "name", element = "External name")
      availabilities <- lAvailabilities(n)
    } yield ExternalPerson(id, name, availabilities)
  }


  private def validate(ep:ExternalPerson): Try[ExternalPerson] ={
    if (checkNonOverlap(ep.availability)) {
      Success(ep)
    } else {
      Failure(new Exception("External person " + ep.id.s + " has overlapped availabilities"))
    }
  }

  def from(id: NonEmptyString, name: NonEmptyString, availability: List[Availability]): Try[ExternalPerson] ={
    if (checkNonOverlap(availability)) {
      Success(ExternalPerson(id,name,availability))
    } else {
      Failure(new Exception("Teacher " + id.s + " has overlapped availabilities"))
    }

  }

  def from(n:Node): Try[ExternalPerson] ={
    val external: Try[ExternalPerson] = for {
      e<-fromXml(n)
      tryE<-validate(e)
    } yield tryE
    external
  }


}