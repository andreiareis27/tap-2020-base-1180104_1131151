package domain.schedule

import scala.util.{Failure, Success, Try}
import scala.xml.Node

case class Teachers(teacherList: List[Teacher])

object Teachers {

  private def fromXml(n: Node): Try[Teachers] = {
    val lt: Try[List[Teacher]] = (n \\ "teacher").foldLeft[Try[List[Teacher]]](Success(Nil)) {
      case (olt, n) => for {
        lt <- olt
        teacher <- Teacher.from(n)
      } yield lt :+ teacher
    }

    lt.map(Teachers(_))
  }

  def from(teacherList: List[Teacher]): Try[Teachers] ={
    if(teacherList.map(t=>t.id).distinct.size==teacherList.size)
      Success(Teachers(teacherList))
    else Failure(new Exception("There are repeated teachers in the Resources list"))
  }

  def from(n: Node): Try[Teachers] = {
    val teachers=for{
      e<-fromXml(n)
      tryE <-from(e.teacherList)
    }yield tryE
    teachers
  }
}
