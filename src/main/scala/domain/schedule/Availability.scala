package domain.schedule

import java.time.LocalDateTime

import scala.util.{Failure, Success, Try}
import scala.xml.Node

case class Availability (start : LocalDateTime, end : LocalDateTime, preference : Int){

  def remove(that:Availability): List[Availability] ={
    val startA = this.start
    val endA = this.end
    val startB = that.start
    val endB = that.end
    val prefA= this.preference

    //check that this contains that
    if ((startA.isBefore(startB)||startA.isEqual(startB)) && (endA.isAfter(endB)||endA.isEqual(endB))) {

      if(startA.isEqual(startB) && endA.isAfter(endB))
        List(Availability(endB,endA,prefA))
      else if(startA.isBefore(startB) && endA.isAfter(endB))
        List(Availability(startA,startB,prefA), Availability(endB, endA, prefA))
      else if(startA.isBefore(startB) && endA.isEqual(endB))
        List(Availability(startA,startB, prefA))
      else List() /*if (startA.isEqual(startB) && endA.isEqual(endB))*/
    }
    else List(this)


  }

}

object Availability {
  private def fromXML(n : Node) : Try[Availability] = {
      for{
        start <- Try( LocalDateTime.parse( (n \@ "start").toString() ) )
        end <- Try( LocalDateTime.parse( (n \@ "end").toString() ) )
        preference <- Try( (n \@ "preference").toInt )
      } yield Availability(start, end, preference)
  }

  def from(start : LocalDateTime, end : LocalDateTime, preference : Int): Try[Availability] = {
    if (start.isAfter(end)) {
      Failure(new Exception("Availability start cannot be after end"))
    } else if (preference < 0) {
      Failure(new Exception("Preference cannot be negative"))
    } else if (start.isEqual(end)) {
      Failure(new Exception("Availability start cannot be equal to end"))
    } else Success(Availability(start, end, preference))
  }

  private def validate(availability: Availability)={
    if (availability.start.isAfter(availability.end)) {
      Failure(new Exception("Availability start cannot be after end"))
    } else if (availability.preference < 0) {
      Failure(new Exception("Preference cannot be negative"))
    } else if (availability.start.isEqual(availability.end)) {
      Failure(new Exception("Availability start cannot be equal to end"))
    } else Success(availability)
  }


  def from(n:Node): Try[Availability] ={
    val tryAvailability= for{
      ava<-fromXML(n)
      tryA<-validate(ava)
    }yield tryA

    tryAvailability match {
      case Success(a)=>Success(a)
      case Failure(exception)=>Failure(new Exception(exception.getMessage))
    }
}
}
