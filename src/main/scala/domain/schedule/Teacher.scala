package domain.schedule

import scala.util.{Failure, Success, Try}
import scala.xml.Node
import assessment.MS03Algorithm.checkNonOverlap

case class Teacher(id: NonEmptyString, name: NonEmptyString, availability: List[Availability]) extends Resource

object Teacher {

  private def lAvailabilities(n: Node): Try[List[Availability]] =
    (n \\ "availability").foldLeft[Try[List[Availability]]](Success(Nil)) {
      case (ols, n) => for {
        lAvailable <- ols
        p <- Availability.from(n)
      } yield lAvailable :+ p
    }

  private def fromXml(n: Node): Try[Teacher] = {
    for {
      id <- NonEmptyString.from(n \@ "id", element = "teacher id")
      name <- NonEmptyString.from(n \@ "name", element = "teacher name")
      availabilities <- lAvailabilities(n)
    } yield Teacher(id, name, availabilities)
  }

  private def validate(t:Teacher): Try[Teacher] ={
    if (checkNonOverlap(t.availability)) {
      Success(t)
    } else {
      Failure(new Exception("Teacher " + t.id.s + " has overlapped availabilities"))
    }
  }

  def from(id: NonEmptyString, name: NonEmptyString, availability: List[Availability]): Try[Teacher] ={
    if (checkNonOverlap(availability)) {
      Success(Teacher(id,name,availability))
    } else {
      Failure(new Exception("Teacher " + id.s + " has overlapped availabilities"))
    }

  }

  def from(n:Node): Try[Teacher] ={
    val teacher: Try[Teacher] = for {
      t<-fromXml(n)
      tryT<-validate(t)
    } yield tryT
    teacher
  }



}