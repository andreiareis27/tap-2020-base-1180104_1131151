package domain.schedule
import java.time.LocalTime
import assessment.Functions.checkInstance

import scala.util.{Failure, Success, Try}
import scala.xml.Node

case class Agenda(vivas: Vivas, resources: Resources, duration: LocalTime)

  object Agenda {


    private def fromXml(n : Node) : Try[Agenda] = {
      for {
        nr <- (n \ "resources").headOption.fold[Try[Node]](Failure(new Exception("node resources does not exist!")))(Success(_))
        resources <- Resources.from(nr)
        nv <- (n \ "vivas").headOption.fold[Try[Node]](Failure(new Exception("node vivas does not exist!")))(Success(_))
        vivas <- Vivas.from(nv, resources)
        duration <- Try(LocalTime.parse((n \@ "duration").toString))
      } yield Agenda(vivas, resources, duration)
    }

    def getResourcesFromViva(viva: Viva): Resources = {
      val coAdvisersViva=viva.coAdvisers
      val getTeachersPresAdv: List[Teacher] = List(viva.president, viva.adviser)
      val getTeachersViva: List[Teacher] = checkInstance(coAdvisersViva).teachers.teacherList ++ getTeachersPresAdv
      val getExternalsViva: List[ExternalPerson] =
        checkInstance(coAdvisersViva).externals.externalList ++ viva.supervisors.externalList
      Resources(Teachers(getTeachersViva), Externals(getExternalsViva))
    }

    def compareDurationWithAvailabilities(v: Viva, d: LocalTime): Boolean = {
      val allResources = getResourcesFromViva(v)
      val teachersViva = allResources.teachers.teacherList
      val externalsViva = allResources.externals.externalList

      val availableTeachers = teachersViva.filter(t => t.availability.exists(a =>
        a.start.plusMinutes((d.getHour * 60) + d.getMinute).isEqual(a.end) ||
          a.start.plusMinutes((d.getHour * 60) + d.getMinute).isBefore(a.end)))

      val availableExternals = externalsViva.filter(e => e.availability.exists(a =>
        a.start.plusMinutes((d.getHour * 60) + d.getMinute).isEqual(a.end) ||
          a.start.plusMinutes((d.getHour * 60) + d.getMinute).isBefore(a.end)))

      (teachersViva.size == availableTeachers.size) && (externalsViva.size == availableExternals.size)
   }

    def from(vivas: Vivas, resources: Resources, duration: LocalTime): Try[Agenda] = {
      if(duration.getHour>5){
        Failure(new Exception("Duration "+duration+" is invalid"))
      } else
        if(!vivas.listVivas.forall(v => compareDurationWithAvailabilities(v, duration))) {
        val message = "Agenda Duration is superior to resources availabilities"
        Failure(new Exception(message))}
      else  {
        Success(Agenda(vivas, resources, duration))
      }
    }


    def from(n:Node): Try[Agenda] ={
      val agenda=for{
        a<-fromXml(n)
        tryA<-from(a.vivas,a.resources,a.duration)
      } yield tryA
      agenda

    }

  }