package assessment

import java.time.{LocalDateTime, LocalTime}

import domain.schedule._

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

object MS03Algorithm {


  def checkNonOverlap(xs: List[Availability]): Boolean = {

    val pairs: List[(Availability, Availability)] = xs.combinations(2).map { case List(a, b) => (a, b) }.toList

    @tailrec
    def checkNoneOverlappingAvailabilities(p: List[(Availability, Availability)]): Boolean = p match {
      case h :: t =>
        if (h._1.start.isAfter(h._2.end) || h._1.end.isBefore(h._2.start)) //if does not overlap...
        checkNoneOverlappingAvailabilities(t)
          else false
      case Nil => true
    }

    checkNoneOverlappingAvailabilities(pairs)

  }

  def checkInstance(resourcesList: List[Resource]): Resources = {

    @scala.annotation.tailrec
    def instance(r: List[Resource], t: List[Teacher], e: List[ExternalPerson]): Resources = {
      r match {
        case h :: tail => h match {
          case Teacher(id, name, availability) =>
            instance(tail, t :+ Teacher(id, name, availability), e)

          case ExternalPerson(id, name, availability) =>
            instance(tail, t, e :+ ExternalPerson(id, name, availability))
        }
        case Nil => Resources(Teachers(t), Externals(e))
      }
    }

    instance(resourcesList, List(), List())
  }


  def minorInterval(xs: List[(LocalDateTime, LocalDateTime, LocalTime)]):
  Option[(LocalDateTime, LocalDateTime, LocalTime)] = {
    @tailrec
    def minAux(m: (LocalDateTime, LocalDateTime, LocalTime), ys: List[(LocalDateTime, LocalDateTime, LocalTime)]):
    Option[(LocalDateTime, LocalDateTime, LocalTime)] = ys match {
      case Nil => Option[(LocalDateTime, LocalDateTime, LocalTime)](m)
      case x :: t =>
        if (x._3.isBefore(m._3))
          minAux(x, t)
        else minAux(m, t)
    }

    if (xs.isEmpty) None
    else {
      val head :: tail = xs
      minAux(head, tail)
    }
  }


  def getOrderedPreferences(listAvailabilities: List[Availability]): List[Availability] = {
    listAvailabilities.sortWith(_.preference < _.preference)
  }

  def getInterval(start: LocalDateTime, end: LocalDateTime): LocalTime = {
    val startTime: LocalTime = start.toLocalTime
    val endTime: LocalTime = end.toLocalTime
    if (endTime.isAfter(startTime))
      endTime.minusSeconds(startTime.toSecondOfDay)
    else
      startTime.minusSeconds(endTime.toSecondOfDay)
  }


  def overlapDateRange(list: List[Availability], vivaDuration: LocalTime): Availability = {
    val s = LocalDateTime.MIN
    val e = LocalDateTime.MIN
    val defaultAvailability = Availability(s, e, 0)
    val head: Availability = list.headOption.fold(defaultAvailability)(a => a)

    val overlappedAvailability: Availability = list.foldLeft(Availability(head.start, head.end, 0)) {
      case (interval: Availability, availability: Availability) =>
        val startA = interval.start
        val endA = interval.end
        val startB = availability.start
        val endB = availability.end

        val ranges: List[(LocalDateTime, LocalDateTime, LocalTime)] =
          List((startA, endA, getInterval(startA, endA)), (startB, endA, getInterval(startB, endA)),
            (startA, endB, getInterval(startA, endB)), (startB, endB, getInterval(startB, endB)))

        val optionOverlap = minorInterval(ranges)
        optionOverlap match {
          case Some(overlap) => {
            if (startA.isBefore(endB) && endA.isAfter(startB) && (overlap._3.compareTo(vivaDuration) >= 0)) {
              Availability(overlap._1, overlap._2, interval.preference + availability.preference)
            }
            else {
              defaultAvailability
            }
          }
          case None => defaultAvailability
        }
    }
    overlappedAvailability
  }


  /*def overlapDateRange2(list: List[Availability], vivaDuration: LocalTime): List[Availability] = {

    val startList = List[Availability]()
    val s = LocalDateTime.MIN
    val e = LocalDateTime.MIN
    val defaultAvailability = Availability(s, e, 0)
    val head: Availability = list.headOption.fold(defaultAvailability)(a => a)

    val overlappedAvailability: Availability = list.foldLeft(Availability(head.start, head.end, 0)) {
      case (interval: Availability, availability: Availability) =>
        val startA = interval.start
        val endA = interval.end
        val startB = availability.start
        val endB = availability.end

        val ranges: List[(LocalDateTime, LocalDateTime, LocalTime)] =
          List((startA, endA, getInterval(startA, endA)), (startB, endA, getInterval(startB, endA)),
            (startA, endB, getInterval(startA, endB)), (startB, endB, getInterval(startB, endB)))

        val optionOverlap = minorInterval(ranges)
        optionOverlap match {
          case Some(overlap) => {
            if (startA.isBefore(endB) && endA.isAfter(startB) && (overlap._3.compareTo(vivaDuration) >= 0)) {
              Availability(overlap._1, overlap._2, interval.preference + availability.preference)
            }
            else {
              defaultAvailability
            }
          }
          case None => defaultAvailability
        }
    }
    startList :+ overlappedAvailability
  }*/


  def getOverlapedAvailabilities(list: List[Availability]) = {
    list.filter(a => a.start == LocalDateTime.MIN)

  }


  def combinationList(la: List[List[Availability]]): List[List[Availability]] = la match {
    case Nil => Nil :: Nil
    case head :: tail =>
      @scala.annotation.tailrec
      val rec = combinationList(tail)
      rec.flatMap(r => head.map(t => t :: r))
  }


  def combinations[A](ll: List[List[A]]): List[List[A]] = {


    def combs[A](xss: List[List[A]]): List[List[A]] = xss match {
      case Nil => List(Nil)
      case xs :: rss =>
        for (x <- xs;
             cs <- combs(rss))
          yield x :: cs
    }

    combs(ll)
  }

  def sortAvailabilitiesByPreference(list: List[List[Availability]])(): List[List[Availability]] = {
    list.sortBy(l=> for{ head<-l.headOption }yield head.start).
      sortBy(l => l.map(a => a.preference).sum)(Ordering[Int].reverse)
    //list.sortBy( l => l.head.start).sortBy(l => l.map(a => a.preference).sum)(Ordering[Int].reverse)
  }




  def combinationsAvailabilitiesOrdered(availabilities: List[List[Availability]]): List[List[Availability]] = {

    val combinations = combinationList(availabilities)
    val sortedList = sortAvailabilitiesByPreference(combinations)

    sortedList
  }

/*
  def sortAvailabilitiesByPreference(list: List[List[Availability]])(): List[List[Availability]] = {
    list.sortBy(
      l => l.map(a => a.preference).sum
    )(Ordering[Int].reverse)
  }*/

  def findVivaswithRepeatedResources(vv: List[(Vivas, Vivas)]) = vv match {
    case h :: t =>
    case Nil =>
  }


  def getTeachersFromViva(viva: Viva): List[Teacher] = {
    val coAd: Resources = checkInstance(viva.coAdvisers)
    val getTeachersPresAdv: List[Teacher] = List(viva.president, viva.adviser)
    val getTeachersViva: List[Teacher] = coAd.teachers.teacherList ++ getTeachersPresAdv
    getTeachersViva
  }

  def getExternalsFromViva(viva: Viva): List[ExternalPerson] = {
    val coAd: Resources = checkInstance(viva.coAdvisers)
    val getExternalsViva: List[ExternalPerson] = coAd.externals.externalList ++ viva.supervisors.externalList
    getExternalsViva
  }

  def getResourcesFromViva(viva: Viva): Resources = {
    val coAd: Resources = checkInstance(viva.coAdvisers)
    val getTeachersPresAdv: List[Teacher] = List(viva.president, viva.adviser)
    val getTeachersViva: List[Teacher] = coAd.teachers.teacherList ++ getTeachersPresAdv
    val getExternalsViva: List[ExternalPerson] = coAd.externals.externalList ++ viva.supervisors.externalList
    Resources(Teachers(getTeachersViva), Externals(getExternalsViva))
  }

  def countResourcesDependencies(v1: Viva, v2: Viva): Int = {
    val nT = getTeachersFromViva(v1).count(t => getTeachersFromViva(v2).exists(t1 => t1.id.s == t.id.s))
    val nE = getExternalsFromViva(v1).count(e => getExternalsFromViva(v2).exists(e1 => e.id.s == e1.id.s))

    nT + nE
  }

  /*def countResourcesDependencies(v1: Viva, v2: Viva): Int = {
    val tch = getResourcesFromViva(v1).teachers.teacherList.zip(getResourcesFromViva(v2).
      teachers.teacherList).count(t => t._1.id.s == t._2.id.s)

    val ext = getResourcesFromViva(v1).externals.externalList.zip(getResourcesFromViva(v2).
      externals.externalList).count(t => t._1.id.s == t._2.id.s)

    tch + ext
  }*/

  def orderVivasByDependency(vivas: List[Viva]): List[Viva] = {
    vivas.sortBy(
      v => vivas.filter(v1 => v.title.s != v1.title.s).map(v1 => countResourcesDependencies(v, v1)).sum
    )
  }


  def getDependenciesForViva(v: Viva, list: List[Viva]): List[Viva] = {
    val dep = list.filter(v1 => getTeachersFromViva(v).exists(t1 => getTeachersFromViva(v1).exists(t2 => t1.id.s == t2.id.s))
      ||  getExternalsFromViva(v).exists(e1 => getExternalsFromViva(v1).exists(e2 => e1.id.s == e2.id.s)))

    if (dep.nonEmpty) v :: dep else dep
  }

  @scala.annotation.tailrec
  def dependenciesGroup(vivas: List[Viva], res: List[List[Viva]]): List[List[Viva]] = vivas match {
    case Nil => res
    case h :: tail => dependenciesGroup(tail, getDependenciesForViva(h, tail) :: res)
  }

  def orderVivasByTitleResourcesAndDependencies(vivas: List[Viva]): List[Viva] = {
    vivas.sortBy(v => vivas.filter(v1 => v.title.s != v1.title.s).sortBy(v => v.title.s).
      map(v1 => countResourcesDependencies(v, v1)).sum).sortBy(v => getTeachersFromViva(v).size +
      getExternalsFromViva(v).size)(Ordering[Int].reverse)
  }


  def getIndependentAndDependentVivas(vs: List[Viva]): List[List[Viva]] = {
    val ind: List[Viva] = vs.filter(v => vs.filter(v1 => v.title.s != v1.title.s).
      map(v1 => countResourcesDependencies(v, v1)).sum == 0)
    val dep: List[Viva] = vs.filter(v => vs.filter(v1 => v.title.s != v1.title.s).
      map(v1 => countResourcesDependencies(v, v1)).sum > 0)

    List(ind, dep)
  }


  def getDependenciesForViva(v: Viva, list: List[Viva], res: List[List[Viva]]): List[List[Viva]] = {
    val dep = list.filter(v1 =>
      getTeachersFromViva(v).exists(t1 => getTeachersFromViva(v1).exists(t2 => t1.id.s == t2.id.s))
      || getExternalsFromViva(v).exists(e1 => getExternalsFromViva(v1).exists(e2 => e1.id.s == e2.id.s)))

    if (res.exists(l => l.exists(v1 => (v1.title.s == v.title.s)))||
      res.exists(l => l.exists(v1 => dep.exists(v2 => v2.title.s == v1.title.s)))){

      val newComb = res.find(l => l.exists(v1 => (v1.title.s == v.title.s) ||
        dep.exists(v2 => v2.title.s == v1.title.s))).get ++ (v :: dep)
      val filterComb = res.filterNot(l => l.exists(v1 => v1.title.s == v.title.s))
      newComb :: filterComb}
      else if (dep.nonEmpty)
      (v :: dep) :: res
      else dep :: res

  }


  def getDependenciesForVivaV2(v: Viva, list: List[Viva], res: List[List[Viva]]): List[List[Viva]] = {
    val dep = list.filter(v1 =>
      getTeachersFromViva(v).exists(t1 => getTeachersFromViva(v1).exists(t2 => t1.id.s == t2.id.s))
        || getExternalsFromViva(v).exists(e1 => getExternalsFromViva(v1).exists(e2 => e1.id.s == e2.id.s)))

    if (res.exists(l => l.exists(v1 => (v1.title.s == v.title.s)))) {
      val newComb = res.find(l => l.exists(v1 => (v1.title.s == v.title.s) ||
        dep.exists(v2 => v2.title.s == v1.title.s))).get ++ dep
      val filterComb = res.filterNot(l => l.exists(v1 => v1.title.s == v.title.s))
      newComb :: filterComb
    } else if (res.exists(l => l.exists(v1 => dep.exists(v2 => v2.title.s == v1.title.s)))){
      val newComb = res.find(l => l.exists(v1 => (v1.title.s == v.title.s) ||
        dep.exists(v2 => v2.title.s == v1.title.s))).get ++ (v :: dep)
      val filterComb = res.filterNot(l => l.exists(v1 => v1.title.s == v.title.s))
      newComb :: filterComb
    } else if (dep.nonEmpty) (v :: dep) :: res else dep :: res

    /* if(res.exists(l => l.exists(v1 => v1.title.s == v.title.s))) {
       for{ r <- res.find(l => l.exists(v1 => v1.title.s == v.title.s)) } yield r :: dep ::
         res.filterNot(l => l.exists(v1 => v1.title.s == v.title.s))
     } else  res*/

  }

  def joinGroups(group: List[Viva], lst: List[List[Viva]]): List[Viva] = {
    val groupJoin = lst.filter(l => l.exists(l1 => group.exists(l2 => l2.title.s == l1.title.s)))
    groupJoin.flatMap(g => group.concat(g).distinct)
  }


  def getAllPermutations[T](l: List[T]): List[List[T]] = l match {
    case List(ele) => List(List(ele))
    case list =>
      for {
        i <- List.range(0, list.length)
        traversedList = list.slice(0, i)
        nextEle = list(i)
        if !(traversedList contains nextEle)
        p <- getAllPermutations(traversedList ++ list.slice(i + 1, list.length))
      } yield list(i) :: p
  }






  def getAllDependenciesGroups(vivas: List[Viva]): List[List[Viva]] = {

    @scala.annotation.tailrec
    def createDependenciesGroups(vivas: List[Viva], res: List[List[Viva]]): List[List[Viva]] = vivas match {
      case Nil => res
      case h :: tail => createDependenciesGroups(tail, getDependenciesForViva(h, tail, res))
    }

    /* @scala.annotation.tailrec
     def createDependenciesGroups(vivas: List[Viva], res: List[List[Viva]]): List[List[Viva]] = vivas match {
       case Nil => res
       case h :: tail => createDependenciesGroups(tail, getDependenciesForViva(h, tail) :: res)
     }*/

    createDependenciesGroups(vivas, List(List())).filter(_.nonEmpty).map(_.distinct)
  }







  def getIndependentVivas(vivas: List[Viva]): List[Viva] = {
    val pairVivas: List[(Viva, Viva)] = vivas.combinations(2).map { case List(a, b) => (a, b) }.toList
    val dep: List[(Viva, Viva)] = pairVivas.filter(
      p => (getResourcesFromViva(p._1).teachers.teacherList.exists(
        t => (getResourcesFromViva(p._2).teachers.teacherList.exists(t1 => t1.id == t.id)) ||
          getResourcesFromViva(p._1).externals.externalList.exists(
            e => (getResourcesFromViva(p._2).externals.externalList.exists(e1 => e1.id == e.id))))
        ))
    //vivas.filterNot(v=>v.title==dep.)

    vivas.filterNot(v => dep.exists(p => p._1.title == v.title || p._2.title == v.title))
    //vivas.filterNot(v=>v.title==dep.)
  }

  def getDependencies(vivas: List[Viva]): Option[(Viva, Viva)] = {
    //vivas.groupBy(v=>v.adviser).toList
    val pairVivas: List[(Viva, Viva)] = vivas.combinations(2).map { case List(a, b) => (a, b) }.toList
    pairVivas.find(p => getResourcesFromViva(p._1).teachers.teacherList.exists(t =>
      getResourcesFromViva(p._2).teachers.teacherList.exists(t1 => t1.id == t.id)) ||
      getResourcesFromViva(p._1).externals.externalList.exists(t =>
        getResourcesFromViva(p._2).externals.externalList.exists(t1 => t1.id == t.id))
    )

  }

  def getDependentVivas(vivas: List[Viva]): List[Viva] = {
    val pairVivas: List[(Viva, Viva)] = vivas.combinations(2).map { case List(a, b) => (a, b) }.toList
    val dep: List[(Viva, Viva)] = pairVivas.filter(
      p => (getResourcesFromViva(p._1).teachers.teacherList.exists(
        t => (getResourcesFromViva(p._2).teachers.teacherList.exists(t1 => t1.id == t.id)) ||
          getResourcesFromViva(p._1).externals.externalList.exists(
            e => (getResourcesFromViva(p._2).externals.externalList.exists(e1 => e1.id == e.id))))
        ))
    //vivas.filterNot(v=>v.title==dep.)

    vivas.filter(v => dep.exists(p => p._1.title == v.title || p._2.title == v.title))
    //vivas.filterNot(v=>v.title==dep.)
  }



  def getAvailabilitiesFromViva(viva: Viva): List[List[Availability]] = {
    val resources = getResourcesFromViva(viva)
    val availabilities = resources.externals.externalList.map(e => e.availability) ++
      resources.teachers.teacherList.map(t => t.availability)
    availabilities
  }


  def combsOverlapped(list: List[List[Availability]], duration: LocalTime): List[Availability] = {
    @scala.annotation.tailrec
    def findCombinationsWithOverlap(combList: List[List[Availability]], resultList: List[Availability]):
    List[Availability] = combList match {

      case avh :: avt =>
        val overlap = overlapDateRange(avh, duration)
        overlap.start match {
          case LocalDateTime.MIN => findCombinationsWithOverlap(avt, resultList)
          case _ => findCombinationsWithOverlap(avt, resultList :+ overlap)

        }
      case Nil => resultList

    }

    findCombinationsWithOverlap(list, List())
  }


  def findFirstOverlap(list: List[List[Availability]], duration: LocalTime): Availability = {

    @scala.annotation.tailrec
    def findCombinationWithOverlap(combList: List[List[Availability]], result: Availability): Availability =
      combList match {

        case avh :: avt =>
          val overlap = overlapDateRange(avh, duration)
          overlap.start match {
            case LocalDateTime.MIN =>
              findCombinationWithOverlap(avt, result)
            case _ => overlap

          }
        case Nil => result

      }

    findCombinationWithOverlap(list, Availability(LocalDateTime.MIN, LocalDateTime.MIN, 0))

  }


  def updateAvailabilities(viva: Viva, fVivas: List[Viva], overlap: Availability) = {
    // val resourcesViva=getResourcesFromViva()

  }

  @scala.annotation.tailrec
  def filterVivas(vivas: List[Viva], resourcesViva: Resources, fVivas: List[Viva], overlap: Availability): List[Viva] = {

    def updateVivaAvailabilities(v: Viva, resourcesCheck: Resources, overLap: Availability): Viva = {

      @tailrec
      def filterCodAdvisersT(codAdvisers: List[Teacher], fList1: List[Teacher]): List[Teacher] = {
        codAdvisers match {
          case Nil => fList1.reverse
          case h :: tail => if (resourcesCheck.teachers.teacherList.exists(_.id == h.id)) {
            filterCodAdvisersT(tail, Teacher(h.id, h.name, filterAvailabilities(h.availability, List())) :: fList1)
          } else {
            filterCodAdvisersT(tail, Teacher(h.id, h.name, h.availability) :: fList1)
          }
        }
      }

      @tailrec
      def filterCodAdvisersE(codAdvisers: List[ExternalPerson], fList1: List[ExternalPerson]): List[ExternalPerson] = {
        codAdvisers match {
          case Nil => fList1.reverse
          case h :: tail => if (resourcesCheck.externals.externalList.exists(_.id == h.id)) {
            filterCodAdvisersE(tail, ExternalPerson(h.id, h.name, filterAvailabilities(h.availability, List())) :: fList1)
          } else {
            filterCodAdvisersE(tail, ExternalPerson(h.id, h.name, h.availability) :: fList1)
          }
        }
      }

      @tailrec
      def filterAvailabilities(avlList: List[Availability], fAvl: List[Availability]): List[Availability] = {
        avlList match {
          case Nil => fAvl.reverse
          case h :: tail => if ((overLap.start.isAfter(h.start) || overLap.start.isEqual(h.start)) &&
            (overLap.end.isBefore(h.end) || overLap.end.isEqual(h.end))) {

            if (h.start.isBefore(overLap.start) && overLap.end.isBefore(h.end)) {
              filterAvailabilities(tail, Availability(overLap.end, h.end, h.preference) ::
                Availability(h.start, overLap.start, h.preference) :: fAvl)
            } else if (h.start.isBefore(overLap.start)) {
              filterAvailabilities(tail, Availability(h.start, overLap.start, h.preference) :: fAvl)
            } else if (overLap.end.isBefore(h.end)) {
              filterAvailabilities(tail, Availability(overLap.end, h.end, h.preference) :: fAvl)
            } else {
              filterAvailabilities(tail, fAvl)
            }
          } else {
            filterAvailabilities(tail, h :: fAvl)
          }
        }
      }

      val fPresident: Teacher = if (resourcesCheck.teachers.teacherList.exists(_.id == v.president.id)) {
        Teacher(v.president.id, v.president.name, filterAvailabilities(v.president.availability, List()))
      } else {
        Teacher(v.president.id, v.president.name, v.president.availability)
      }

      val fAdviser = if (resourcesCheck.teachers.teacherList.exists(_.id == v.adviser.id)) {
        Teacher(v.adviser.id, v.adviser.name, filterAvailabilities(v.adviser.availability, List()))
      } else {
        Teacher(v.adviser.id, v.adviser.name, v.adviser.availability)
      }

      val coAd = checkInstance(v.coAdvisers)
      val fcodAdvisersT: List[Teacher] = filterCodAdvisersT(coAd.teachers.teacherList, List())
      val fcodAdvisersE: List[ExternalPerson] = filterCodAdvisersE(coAd.externals.externalList, List())

      val fSuper: Externals = Externals(filterCodAdvisersE(v.supervisors.externalList, List()))

      Viva(v.title, v.student, fPresident, fAdviser, fcodAdvisersT ++ fcodAdvisersE, fSuper)
    }

    vivas match {
      case Nil => fVivas
      case h :: tail => filterVivas(tail, resourcesViva, updateVivaAvailabilities(h, resourcesViva, overlap) :: fVivas, overlap)
    }
  }


  def getScheduledVivas(lVivas: List[Viva], d: LocalTime): List[ScheduledViva] = {

    @scala.annotation.tailrec
    def getScheduledVivasFromVivas(vivas: List[Viva], duration: LocalTime, schedules: List[ScheduledViva]):
    List[ScheduledViva] =
      vivas match {
        case hv :: tv => {
          val availabilities = getAvailabilitiesFromViva(hv)
          val combsAvailabilities = combinationsAvailabilitiesOrdered(availabilities)

          val overLap: Availability = findFirstOverlap(combsAvailabilities, duration)

          val resourcesViva: Resources = getResourcesFromViva(hv)

          val nOverLap: Availability = Availability(overLap.start, overLap.start.plusMinutes((duration.getHour * 60)
            + duration.getMinute), overLap.preference)

          val fList = filterVivas(tv, resourcesViva, List(), nOverLap).reverse

          val scheduledViva = ScheduledViva(nOverLap.start, nOverLap.end, hv, nOverLap.preference)

          getScheduledVivasFromVivas(fList, duration, schedules :+ scheduledViva)
        }

        case Nil => schedules
      }

    val scheduledVivas = getScheduledVivasFromVivas(lVivas, d, List()).
      sortBy(sh => sh.viva.title.s).sortBy(sh => sh.start)
    scheduledVivas
  }

  def createCompleteSchedule(agenda: Agenda): CompleteSchedule = {
    val vivas: List[Viva] = agenda.vivas.listVivas
    val depVivas: List[List[Viva]] = getIndependentAndDependentVivas(vivas)
    val depGroups: List[List[Viva]] = getAllDependenciesGroups(depVivas.last).map(_.sortBy(_.title.s))


    val schIndp: List[ScheduledViva] = getScheduledVivas(depVivas.head,agenda.duration)

    val schDep: List[List[ScheduledViva]] = depGroups.flatMap(_.permutations.toList.
      map(v => getScheduledVivas(v,agenda.duration)))

    val schDep2: List[ScheduledViva] = depGroups.flatMap(_.permutations.toList.map((v: List[Viva]) =>
      getScheduledVivas(v,agenda.duration)).
      sortBy((l: List[ScheduledViva]) => l.map(x => x.preference).sum)(Ordering[Int]).head)

    val bestComb: List[ScheduledViva] = schDep.sortBy(l => l.map(x => x.preference).sum)(Ordering[Int].reverse).head.
      sortBy(sh => sh.viva.title.s).sortBy(sh => sh.start)

    /*
    * val vivas: List[Viva] = agenda.vivas.listVivas
    val depVivas: List[List[Viva]] = getIndependentAndDependentVivas(vivas)
    val depGroups: List[List[Viva]] = getAllDependenciesGroups(depVivas.last).map(_.sortBy(_.title.s))

    val schIndp: List[ScheduledViva] = getScheduledVivas(depVivas.head, agenda.duration)

    val schDep: List[ScheduledViva] = depGroups.flatMap( g => getAllPermutations(g).map(v =>
      getScheduledVivas(v, agenda.duration)).sortBy(l => l.map(x => x.preference).sum)(Ordering[Int]).head)

    val bestComb: List[ScheduledViva] = (schDep ++ schIndp).sortBy(sh => sh.viva.title.s).sortBy(sh => sh.start)

    CompleteSchedule(bestComb)
    * */


    CompleteSchedule(bestComb)
  }

  def apply(agenda: Agenda): Try[CompleteSchedule] = {

    val res: CompleteSchedule = createCompleteSchedule(agenda)

    val find: Option[ScheduledViva] = res.scheduledVivaList.find(x => x.start == LocalDateTime.MIN)
    find match {
      case Some(sv) => Failure(new Exception("Viva " + sv.viva.title.s + " cannot be scheduled"))
      case None => Success(res)
    }

  }


}