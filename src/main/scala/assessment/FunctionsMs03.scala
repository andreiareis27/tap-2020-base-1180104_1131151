package assessment

import java.time.{LocalDateTime, LocalTime}

import domain.schedule._

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

object FunctionsMs03 {

  def checkInstance(resourcesList: List[Resource]): Resources = {

    @scala.annotation.tailrec
    def instance(r: List[Resource], t: List[Teacher], e: List[ExternalPerson]): Resources = {
      r match {
        case h :: tail => h match {
          case Teacher(id, name, availability) =>
            instance(tail, t :+ Teacher(id, name, availability), e)
          case ExternalPerson(id, name, availability) =>
            instance(tail, t, e :+ ExternalPerson(id, name, availability))
        }
        case Nil => Resources(Teachers(t), Externals(e))
      }
      //Resources(Teachers(t),Externals(e))
    }

    instance(resourcesList, List(), List())
  }

  def getTeachersFromViva(viva: Viva): List[Teacher] = {
    val coAd: Resources = checkInstance(viva.coAdvisers)
    val getTeachersPresAdv: List[Teacher] = List(viva.president, viva.adviser)
    val getTeachersViva: List[Teacher] = coAd.teachers.teacherList ++ getTeachersPresAdv
    getTeachersViva
  }

  def getExternalsFromViva(viva: Viva): List[ExternalPerson] = {
    val coAd: Resources = checkInstance(viva.coAdvisers)
    val getExternalsViva: List[ExternalPerson] = coAd.externals.externalList ++ viva.supervisors.externalList
    getExternalsViva
  }

  def countResourcesDependencies(v1: Viva, v2: Viva): Int = {
    val nT = getTeachersFromViva(v1).count(t => getTeachersFromViva(v2).exists(t1 => t1.id.s == t.id.s))
    val nE = getExternalsFromViva(v1).count(e => getExternalsFromViva(v2).exists(e1 => e.id.s == e1.id.s))

    nT + nE
  }

  def orderVivasByDependency(vivas: List[Viva]): List[Viva] = {
    vivas.sortBy(
      v => vivas.filter(v1 => v.title.s != v1.title.s).map(v1 => countResourcesDependencies(v, v1)).sum
    )
  }

  def orderVivasByTitleResourcesAndDependencies(vivas: List[Viva]): List[Viva] = {
    vivas.sortBy(v => vivas.filter(v1 => v.title.s != v1.title.s).sortBy(v => v.title.s).
      map(v1 => countResourcesDependencies(v, v1)).sum).sortBy(v => getTeachersFromViva(v).size +
      getExternalsFromViva(v).size)(Ordering[Int].reverse)
  }

  def getDependentAndIndependentVivas(vs: List[Viva]): List[List[Viva]] = {
    val ind = vs.filter(v => vs.filter(v1 => v.title.s != v1.title.s).map(v1 => countResourcesDependencies(v, v1)).sum == 0)
    val dep = vs.filter(v => vs.filter(v1 => v.title.s != v1.title.s).map(v1 => countResourcesDependencies(v, v1)).sum > 0)

    List(ind, dep)
  }

  /*def getDependenciesForViva(v: Viva, list: List[Viva]): List[Viva] = {
    val dep = list.filter(v1 => getTeachersFromViva(v).exists(t1 => getTeachersFromViva(v1).exists(t2 => t1.id.s ==
      t2.id.s)) || getExternalsFromViva(v).exists(e1 => getExternalsFromViva(v1).exists(e2 => e1.id.s == e2.id.s)))

    if (dep.nonEmpty) v :: dep else dep
  }*/

  def getDependenciesForViva(v: Viva, list: List[Viva], res: List[List[Viva]]): List[List[Viva]] = {
    val dep = list.filter(v1 => getTeachersFromViva(v).exists(t1 => getTeachersFromViva(v1).exists(t2 => t1.id.s ==
      t2.id.s)) || getExternalsFromViva(v).exists(e1 => getExternalsFromViva(v1).exists(e2 => e1.id.s == e2.id.s)))

    if (res.exists(l => l.exists(v1 => (v1.title.s == v.title.s)))) {
      val newComb = res.find(l => l.exists(v1 => (v1.title.s == v.title.s) ||
        dep.exists(v2 => v2.title.s == v1.title.s))).get ++ dep
      val filterComb = res.filterNot(l => l.exists(v1 => v1.title.s == v.title.s))
      newComb :: filterComb
    } else if (res.exists(l => l.exists(v1 => dep.exists(v2 => v2.title.s == v1.title.s)))){
      val newComb = res.find(l => l.exists(v1 => (v1.title.s == v.title.s) ||
        dep.exists(v2 => v2.title.s == v1.title.s))).get ++ (v :: dep)
      val filterComb = res.filterNot(l => l.exists(v1 => v1.title.s == v.title.s))
      newComb :: filterComb
    } else if (dep.nonEmpty) (v :: dep) :: res else dep :: res

   /* if(res.exists(l => l.exists(v1 => v1.title.s == v.title.s))) {
      for{ r <- res.find(l => l.exists(v1 => v1.title.s == v.title.s)) } yield r :: dep ::
        res.filterNot(l => l.exists(v1 => v1.title.s == v.title.s))
    } else  res*/
  }

  def getAllDependenciesGroups(vivas: List[Viva]): List[List[Viva]] = {

     @scala.annotation.tailrec
    def createDependenciesGroups(vivas: List[Viva], res: List[List[Viva]]): List[List[Viva]] = vivas match {
      case Nil => res
      case h :: tail => createDependenciesGroups(tail, getDependenciesForViva(h, tail, res))
    }

   /* @scala.annotation.tailrec
    def createDependenciesGroups(vivas: List[Viva], res: List[List[Viva]]): List[List[Viva]] = vivas match {
      case Nil => res
      case h :: tail => createDependenciesGroups(tail, getDependenciesForViva(h, tail) :: res)
    }*/

    createDependenciesGroups(vivas, List(List())).filter(_.nonEmpty).map(_.distinct)
  }

  def getAllPermutations[T](l: List[T]): List[List[T]] = l match {
    case List(ele) => List(List(ele))
    case list =>
      for {
        i <- List.range(0, list.length)
        traversedList = list.slice(0, i)
        nextEle = list(i)
        if !(traversedList contains nextEle)
        p <- getAllPermutations(traversedList ++ list.slice(i + 1, list.length))
      } yield list(i) :: p
  }

  def combinationsAvailabilities(availabilities: List[List[Availability]]): List[List[Availability]] = {
    val combinations = combinationList(availabilities)
    val sortedList = sortAvailabilitiesByPreference(combinations)

    sortedList
  }

  def combinationList(la: List[List[Availability]]): List[List[Availability]] = la match {
    case Nil => Nil :: Nil
    case head :: tail =>
      @scala.annotation.tailrec
      val rec = combinationList(tail)
      rec.flatMap(r => head.map(t => t :: r))
  }

  def sortAvailabilitiesByPreference(list: List[List[Availability]])(): List[List[Availability]] = {
    list.sortBy(l => l.head.start).sortBy(l => l.map(a => a.preference).sum)(Ordering[Int].reverse)
  }

  def minorInterval(xs: List[(LocalDateTime, LocalDateTime, LocalTime)]): Option[(LocalDateTime, LocalDateTime, LocalTime)] = {
    @tailrec
    def minAux(m: (LocalDateTime, LocalDateTime, LocalTime), ys: List[(LocalDateTime, LocalDateTime, LocalTime)]):
    Option[(LocalDateTime, LocalDateTime, LocalTime)] = ys match {
      case Nil => Option[(LocalDateTime, LocalDateTime, LocalTime)](m)
      case x :: t =>
        if (x._3.isBefore(m._3))
          minAux(x, t)
        else minAux(m, t)
    }

    if (xs.isEmpty) None
    else {
      val head :: tail = xs
      minAux(head, tail)
    }
  }

  def getAvailabilityMajorPreference(xs: List[Availability]): Option[Availability] = {
    @tailrec
    def minAux(m: Availability, ys: List[Availability]): Option[Availability] = ys match {
      case Nil => Option[Availability](m)
      case x :: t =>
        if (x.preference < m.preference)
          minAux(x, t)
        else minAux(m, t)
    }

    if (xs.isEmpty) None
    else {
      val head :: tail = xs
      minAux(head, tail)
    }
  }

  def getOrderedPreferences(listAvailabilities: List[Availability]): List[Availability] = {
    listAvailabilities.sortWith(_.preference < _.preference)
  }

  def getInterval(start: LocalDateTime, end: LocalDateTime): LocalTime = {
    val startTime: LocalTime = start.toLocalTime
    val endTime: LocalTime = end.toLocalTime
    if (endTime.isAfter(startTime))
      endTime.minusSeconds(startTime.toSecondOfDay)
    else
      startTime.minusSeconds(endTime.toSecondOfDay)
  }


  def overlapDateRange(list: List[Availability], vivaDuration: LocalTime): Availability = {
    val s = LocalDateTime.MIN
    val e = LocalDateTime.MIN
    val defaultAvailability = Availability(s, e, 0)
    val head: Availability = list.headOption.fold(defaultAvailability)(a => a)

    val overlappedAvailability: Availability = list.foldLeft(Availability(head.start, head.end, 0)) {
      case (interval: Availability, availability: Availability) =>
        val startA = interval.start
        val endA = interval.end
        val startB = availability.start
        val endB = availability.end

        val ranges: List[(LocalDateTime, LocalDateTime, LocalTime)] =
          List((startA, endA, getInterval(startA, endA)), (startB, endA, getInterval(startB, endA)),
            (startA, endB, getInterval(startA, endB)), (startB, endB, getInterval(startB, endB)))

        val optionOverlap = minorInterval(ranges)
        optionOverlap match {
          case Some(overlap) =>
            if (startA.isBefore(endB) && endA.isAfter(startB) && (overlap._3.compareTo(vivaDuration) >= 0)) {
              Availability(overlap._1, overlap._2, interval.preference + availability.preference)
            }
            else {
              defaultAvailability
            }
          case None => defaultAvailability
        }
    }
    overlappedAvailability
  }

  def createCompleteSchedule(agenda: Agenda): CompleteSchedule = {

    @scala.annotation.tailrec
    def createScheduledVivas(vivas: List[Viva], schedules: List[ScheduledViva], index: Int): List[ScheduledViva] = {

      @scala.annotation.tailrec
      def findOverlapCombination(combList: List[List[Availability]], result: Availability, comb: List[Availability]): Availability = {
        if (result.start == LocalDateTime.MIN) {
          combList match {
            case Nil => result
            case h :: tail => findOverlapCombination(tail, overlapDateRange(h, agenda.duration), h)
          }
        } else {
          if (comb.exists(f => f.start.isAfter(result.start))) {
            Availability(comb.find(f => f.start.isAfter(result.start)).get.start, result.end, result.preference)
          } else {
            result
          }
        }
      }

      if (index == vivas.size) {
        schedules
      } else {

        val viva: Viva = vivas(index)

        val coAdv = checkInstance(viva.coAdvisers)

        val getTeachersPresAdv: List[Teacher] = List(viva.president, viva.adviser)

        val getTeachersViva: List[Teacher] = coAdv.teachers.teacherList ++ getTeachersPresAdv

        val getExternalsViva: List[ExternalPerson] = coAdv.externals.externalList ++ viva.supervisors.externalList

        val availabilitiesExternals: List[List[Availability]] = getExternalsViva.map(x => x.availability)
        val availabilitiesTeachers: List[List[Availability]] = getTeachersViva.map(x => x.availability)

        val availabilitiesResourcesViva: List[List[Availability]] = availabilitiesTeachers ++ availabilitiesExternals

        val combinationsAvs = combinationsAvailabilities(availabilitiesResourcesViva)

        val overLap: Availability = findOverlapCombination(combinationsAvs,
          Availability(LocalDateTime.MIN, LocalDateTime.MIN, 0), List())

        val nOverLap: Availability = Availability(overLap.start, overLap.start.plusMinutes((agenda.duration.getHour * 60)
          + agenda.duration.getMinute), overLap.preference)

        val resourcesViva: Resources = Resources(Teachers(getTeachersViva), Externals(getExternalsViva))

        val fList = filterVivas(vivas, resourcesViva, List(), nOverLap).reverse

        val schedule: ScheduledViva = ScheduledViva(nOverLap.start, nOverLap.end, fList(index), nOverLap.preference)

        createScheduledVivas(fList, schedule :: schedules, index + 1)
      }
    }

    val vivas: List[Viva] = agenda.vivas.listVivas
    val depVivas: List[List[Viva]] = getDependentAndIndependentVivas(vivas)
    val depGroups: List[List[Viva]] = getAllDependenciesGroups(depVivas.last).map(_.sortBy(_.title.s))

    val schIndp: List[ScheduledViva] = createScheduledVivas(depVivas.head, List(), 0).reverse

    val schDep: List[ScheduledViva] = depGroups.flatMap( g => getAllPermutations(g).map(v =>
      createScheduledVivas(v, List(), 0)).sortBy(l => l.map(x => x.preference).sum)(Ordering[Int]).head)

    val bestComb = (schDep ++ schIndp).sortBy(sh => sh.viva.title.s).sortBy(sh => sh.start)

    CompleteSchedule(bestComb)

 /*   val cv = vivas.permutations.toList

    val sch2: List[CompleteSchedule] = cv.map(v => CompleteSchedule(createScheduledVivas(v, List(), 0).reverse.
      sortBy(sh => sh.viva.title.s).sortBy(sh => sh.start)))

    val teste: CompleteSchedule = sch2.sortBy(s => s.scheduledVivaList.map(x => x.preference).sum)(Ordering[Int].reverse).head

    teste*/

    /* val x = depGroups.map(_.permutations.toList.map(v => createScheduledVivas(v, List(), 0)).sortBy(l =>
      l.map(x => x.preference).sum)(Ordering[Int]).head)*/

   /* val x = depGroups.map(_.permutations.toList.map(v => createScheduledVivas(v, List(), 0)))

    val schDep: List[ScheduledViva] = depGroups.flatMap(_.permutations.toList.map(v => createScheduledVivas(v, List(),
                                        0)).sortBy(l => l.map(x => x.preference).sum)(Ordering[Int]).head)

    val bestComb = (schDep ++ schIndp).sortBy(sh => sh.viva.title.s).sortBy(sh => sh.start)

    CompleteSchedule(bestComb)*/

   /* val cv = vivas.permutations.toList

//    val sch = createScheduledVivas(vivas, List(), 0).reverse.sortBy(sh => sh.viva.title.s).sortBy(sh => sh.start)
    val sch2: List[CompleteSchedule] = cv.map(v => CompleteSchedule(createScheduledVivas(v, List(), 0).reverse.
      sortBy(sh => sh.viva.title.s).sortBy(sh => sh.start)))

    val teste: CompleteSchedule = sch2.sortBy(s => s.scheduledVivaList.map(x => x.preference).sum)(Ordering[Int].reverse).head

    teste*/
  }

  @scala.annotation.tailrec
  def filterVivas(vivas: List[Viva], resourcesViva: Resources, fVivas: List[Viva], overlap: Availability): List[Viva] = {

    def updateVivaAvailabilities(v: Viva, resourcesCheck: Resources, overLap: Availability): Viva = {

      @tailrec
      def filterCodAdvisersT(codAdvisers: List[Teacher], fList1: List[Teacher]): List[Teacher] = {
        codAdvisers match {
          case Nil => fList1.reverse
          case h :: tail => if (resourcesCheck.teachers.teacherList.exists(_.id == h.id)) {
            filterCodAdvisersT(tail, Teacher(h.id, h.name, filterAvailabilities(h.availability, List())) :: fList1)
          } else {
            filterCodAdvisersT(tail, Teacher(h.id, h.name, h.availability) :: fList1)
          }
        }
      }

      @tailrec
      def filterCodAdvisersE(codAdvisers: List[ExternalPerson], fList1: List[ExternalPerson]): List[ExternalPerson] = {
        codAdvisers match {
          case Nil => fList1.reverse
          case h :: tail => if (resourcesCheck.externals.externalList.exists(_.id == h.id)) {
            filterCodAdvisersE(tail, ExternalPerson(h.id, h.name, filterAvailabilities(h.availability, List())) :: fList1)
          } else {
            filterCodAdvisersE(tail, ExternalPerson(h.id, h.name, h.availability) :: fList1)
          }
        }
      }

      @tailrec
      def filterAvailabilities(avlList: List[Availability], fAvl: List[Availability]): List[Availability] = {
        avlList match {
          case Nil => fAvl.reverse
          case h :: tail => if ((overLap.start.isAfter(h.start) || overLap.start.isEqual(h.start)) &&
            (overLap.end.isBefore(h.end) || overLap.end.isEqual(h.end))) {

            if (h.start.isBefore(overLap.start) && overLap.end.isBefore(h.end)) {
              filterAvailabilities(tail, Availability(overLap.end, h.end, h.preference) ::
                Availability(h.start, overLap.start, h.preference) :: fAvl)
            } else if (h.start.isBefore(overLap.start)) {
              filterAvailabilities(tail, Availability(h.start, overLap.start, h.preference) :: fAvl)
            } else if (overLap.end.isBefore(h.end)) {
              filterAvailabilities(tail, Availability(overLap.end, h.end, h.preference) :: fAvl)
            } else {
              filterAvailabilities(tail, fAvl)
            }
          } else {
            filterAvailabilities(tail, h :: fAvl)
          }
        }
      }

      val fPresident: Teacher = if (resourcesCheck.teachers.teacherList.exists(_.id == v.president.id)) {
        Teacher(v.president.id, v.president.name, filterAvailabilities(v.president.availability, List()))
      } else {
        Teacher(v.president.id, v.president.name, v.president.availability)
      }

      val fAdviser = if (resourcesCheck.teachers.teacherList.exists(_.id == v.adviser.id)) {
        Teacher(v.adviser.id, v.adviser.name, filterAvailabilities(v.adviser.availability, List()))
      } else {
        Teacher(v.adviser.id, v.adviser.name, v.adviser.availability)
      }

      val coAd = checkInstance(v.coAdvisers)
      val fcodAdvisersT: List[Teacher] = filterCodAdvisersT(coAd.teachers.teacherList, List())
      val fcodAdvisersE: List[ExternalPerson] = filterCodAdvisersE(coAd.externals.externalList, List())

      val fCod: Resources = Resources(Teachers(fcodAdvisersT), Externals(fcodAdvisersE))

      val fSuper: Externals = Externals(filterCodAdvisersE(v.supervisors.externalList, List()))

      Viva(v.title, v.student, fPresident, fAdviser, fcodAdvisersT ++ fcodAdvisersE, fSuper)
    }

    vivas match {
      case Nil => fVivas
      case h :: tail => filterVivas(tail, resourcesViva, updateVivaAvailabilities(h, resourcesViva, overlap) :: fVivas, overlap)
    }
  }

  def apply(agenda: Agenda)(): Try[CompleteSchedule] = {
    val res: CompleteSchedule = createCompleteSchedule(agenda)

    val find: Option[ScheduledViva] = res.scheduledVivaList.find(x => x.start == LocalDateTime.MIN)
    find match {
      case Some(sv) => Failure(new Exception("Viva " + sv.viva.title.s + " cannot be scheduled"))
      case None => Success(res)
    }
  }

}

