package assessment

import java.time.{LocalDateTime, LocalTime}

import domain.schedule._

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

object Functions {

  //Combinações Availabilities
  def combinationsAvailabilities(availabilities: List[List[Availability]]): List[List[Availability]] = {

    val combinations = combinationList(availabilities)
    //    val filterCombinations = filterResults(combinations, List(List()), 0)
    //    val nfilter = filterCombinations.filter(_.nonEmpty)
    val sortedList = sortAvailabilitiesByDate(combinations)

    sortedList
  }

  def combinationList(la: List[List[Availability]]): List[List[Availability]] = la match {
    case Nil => Nil :: Nil
    case head :: tail =>
      @scala.annotation.tailrec
      val rec = combinationList(tail)
      rec.flatMap(r => head.map(t => t :: r))
  }

  @scala.annotation.tailrec
  def filterResults(combList: List[List[Availability]], filterList: List[List[Availability]], index: Int)()
  : List[List[Availability]] = {

    if (index == combList.size) {
      filterList
    } else {

      val flag = checkDuplicateResults(combList, combList(index), index + 1, 0)

      if (flag == 0) {
        filterResults(combList, combList(index) :: filterList, index + 1)
      } else {
        filterResults(combList, filterList, index + 1)
      }
    }
  }

  @scala.annotation.tailrec
  def checkDuplicateResults(combList: List[List[Availability]], listToCheck: List[Availability], index: Int, flag: Int)(): Int = {
    if (index == combList.size || flag == 1) {
      flag
    } else {
      val ch = for {x <- combList(index) ++ listToCheck
                    if combList(index).forall(_.preference != x.preference) ||
                      listToCheck.forall(_.preference != x.preference)
                    } yield x

      if (ch.nonEmpty) {
        checkDuplicateResults(combList, listToCheck, index + 1, 0)
      } else {
        checkDuplicateResults(combList, listToCheck, index + 1, 1)
      }
    }
  }

  def sortAvailabilitiesByDate(list: List[List[Availability]])(): List[List[Availability]] = {
    list.sortBy(
      l => l.map(a => a.start.getHour).sum
    )
    /*   list.sortBy(
         l => l.map(a => a.preference).sum
       )*/
  }

  def minorInterval(xs: List[(LocalDateTime, LocalDateTime, LocalTime)]):
  Option[(LocalDateTime, LocalDateTime, LocalTime)] = {
    @tailrec
    def minAux(m: (LocalDateTime, LocalDateTime, LocalTime), ys: List[(LocalDateTime, LocalDateTime, LocalTime)]):
    Option[(LocalDateTime, LocalDateTime, LocalTime)] = ys match {
      case Nil => Option[(LocalDateTime, LocalDateTime, LocalTime)](m)
      case x :: t =>
        if (x._3.isBefore(m._3))
          minAux(x, t)
        else minAux(m, t)
    }

    if (xs.isEmpty) None
    else {
      val head :: tail = xs
      minAux(head, tail)
    }
  }

  def getAvailabilityMajorPreference(xs: List[Availability]): Option[Availability] = {
    @tailrec
    def minAux(m: Availability, ys: List[Availability]): Option[Availability] = ys match {
      case Nil => Option[Availability](m)
      case x :: t =>
        if (x.preference < m.preference)
          minAux(x, t)
        else minAux(m, t)
    }

    if (xs.isEmpty) None
    else {
      val head :: tail = xs
      minAux(head, tail)
    }
  }

  def getOrderedPreferences(listAvailabilities: List[Availability]): List[Availability] = {
    listAvailabilities.sortWith(_.preference < _.preference)
  }

  def getInterval(start: LocalDateTime, end: LocalDateTime): LocalTime = {
    val startTime: LocalTime = start.toLocalTime
    val endTime: LocalTime = end.toLocalTime
    if (endTime.isAfter(startTime))
      endTime.minusSeconds(startTime.toSecondOfDay)
    else
      startTime.minusSeconds(endTime.toSecondOfDay)
  }


  def overlapDateRange(list: List[Availability], vivaDuration: LocalTime): Availability = {
    val s = LocalDateTime.MIN
    val e = LocalDateTime.MIN
    val defaultAvailability = Availability(s, e, 0)
    val head: Availability =list.headOption.fold(defaultAvailability)(a=>a)

    val overlappedAvailability: Availability = list.foldLeft(Availability(head.start, head.end, 0)) {
      case (interval: Availability, availability: Availability) =>
        val startA = interval.start
        val endA = interval.end
        val startB = availability.start
        val endB = availability.end

        val ranges: List[(LocalDateTime, LocalDateTime, LocalTime)] =
          List((startA, endA, getInterval(startA, endA)), (startB, endA, getInterval(startB, endA)),
            (startA, endB, getInterval(startA, endB)),(startB, endB, getInterval(startB, endB)))

        val optionOverlap= minorInterval(ranges)
        optionOverlap match {
          case Some(overlap) => {
            if (startA.isBefore(endB) && endA.isAfter(startB) && (overlap._3.compareTo(vivaDuration) >= 0)) {
              Availability(overlap._1, overlap._2, interval.preference + availability.preference)
            }
            else {
              defaultAvailability
            }
          }
          case None => defaultAvailability
        }
    }
    overlappedAvailability
  }

  /*def overlapDateRange2(list: List[Availability], vivaDuration: LocalTime): List[Availability] = {

    val s = LocalDateTime.MIN
    val e = LocalDateTime.MIN
    val startList = List[Availability]()

    val initialAvailability = Availability(s, e, 0)

    val overlappedAvailability: Availability = list.foldLeft(initialAvailability) {
      case (interval: Availability, availability: Availability) =>
        val startA = interval.start
        val endA = interval.end
        val startB = availability.start
        val endB = availability.end

        val ranges: List[ScheduledDuration] =
          List(ScheduledDuration(startA, endA, getInterval(startA, endA)),
            ScheduledDuration(startB, endA, getInterval(startB, endA)),
            ScheduledDuration(startA, endB, getInterval(startA, endB)),
            ScheduledDuration(startB, endB, getInterval(startB, endB)))

        // val ranges: List[ScheduledDuration]=
        //  List(getDuration(startA,endA),getDuration(startB,endA),getDuration(startA,endB),getDuration(startB,endB))
        val overlap= minorInterval(ranges).get
        if (startA.isBefore(endB) && endA.isAfter(startB) && (overlap.duration.compareTo(vivaDuration) >= 0)) {
          Availability(overlap.start, overlap.end, interval.preference + availability.preference)
        }
        else {
          //availability
          initialAvailability
        }
      //somar as preferencias das disponibilidades que coincidirem
      //val newScheduledViva=ScheduledViva()
    }
    startList :+ overlappedAvailability
  }*/

  def checkInstance(resourcesList:List[Resource]): Resources ={

    @scala.annotation.tailrec
    def instance(r:List[Resource], t:List[Teacher], e:List[ExternalPerson]):Resources={
      r match {
        case h::tail=> h match {
          case Teacher(id,name,availability)=>{
            instance(tail,t:+Teacher(id,name,availability),e)
          }
          case ExternalPerson(id,name,availability)=>{
            instance(tail,t,e:+ExternalPerson(id,name,availability))
          }
        }
        case  Nil=> Resources(Teachers(t),Externals(e))
      }
      //Resources(Teachers(t),Externals(e))
    }

    instance(resourcesList,List(),List())

  }

  def createCompleteSchedule(agenda: Agenda): CompleteSchedule = {

    @scala.annotation.tailrec
    def createScheduledVivas(vivas: List[Viva], schedules: List[ScheduledViva], index: Int): List[ScheduledViva] = {

      @scala.annotation.tailrec
      def findOverlapCombination(combList: List[List[Availability]], result: Availability, comb: List[Availability]): Availability = {
        if (result.start == LocalDateTime.MIN) {
          combList match {
            case Nil => result
            case h :: tail => findOverlapCombination(tail, overlapDateRange(h, agenda.duration), h)
          }
        } else {
          if (comb.exists(f => f.start.isAfter(result.start))) {
            Availability(comb.find(f => f.start.isAfter(result.start)).get.start, result.end, result.preference)
          } else {
            result
          }
        }
      }

      if (index == vivas.size) {
        schedules
      } else {

        val viva: Viva = vivas(index)

        //val getTeachersPresAdv: List[Teacher] = List(viva.president.get, viva.adviser.get
        val coAdvisers=viva.coAdvisers

        val getTeachersPresAdv: List[Teacher] = List(viva.president, viva.adviser)

        val getTeachersViva: List[Teacher] = checkInstance(coAdvisers).teachers.teacherList ++ getTeachersPresAdv

        val getExternalsViva: List[ExternalPerson] = checkInstance(coAdvisers).externals.externalList ++
                                                      viva.supervisors.externalList

        val availabilitiesExternals: List[List[Availability]] = getExternalsViva.map(x => x.availability)
        val availabilitiesTeachers: List[List[Availability]] = getTeachersViva.map(x => x.availability)

        val availabilitiesResourcesViva: List[List[Availability]] = availabilitiesTeachers ++ availabilitiesExternals

        val combinationsAvs = combinationsAvailabilities(availabilitiesResourcesViva)

        val overLap: Availability = findOverlapCombination(combinationsAvs,
          Availability(LocalDateTime.MIN, LocalDateTime.MIN, 0), List())

        val nOverLap: Availability = Availability(overLap.start, overLap.start.plusMinutes((agenda.duration.getHour * 60)
          + agenda.duration.getMinute), overLap.preference)

        val resourcesViva: Resources = Resources(Teachers(getTeachersViva), Externals(getExternalsViva))

        val fList = filterVivas(vivas, resourcesViva, List(), nOverLap).reverse

        val schedule: ScheduledViva = ScheduledViva(nOverLap.start, nOverLap.end, fList(index), nOverLap.preference)

        createScheduledVivas(fList, schedule :: schedules, index + 1)
      }
    }


    val vivas: List[Viva] = agenda.vivas.listVivas
    val sch = createScheduledVivas(vivas, List(), 0).reverse

    CompleteSchedule(sch)
  }

  @scala.annotation.tailrec
  def filterVivas(vivas: List[Viva], resourcesViva: Resources, fVivas: List[Viva], overlap: Availability): List[Viva] = {

    def updateVivaAvailabilities(v: Viva, resourcesCheck: Resources, overLap: Availability): Viva = {

      @tailrec
      def filterCodAdvisersT(codAdvisers: List[Teacher], fList1: List[Teacher]): List[Teacher] = {
        codAdvisers match {
          case Nil => fList1.reverse
          case h :: tail => if (resourcesCheck.teachers.teacherList.exists(_.id == h.id)) {
            filterCodAdvisersT(tail, Teacher(h.id, h.name, filterAvailabilities(h.availability, List())) :: fList1)
          } else {
            filterCodAdvisersT(tail, Teacher(h.id, h.name, h.availability) :: fList1)
          }
        }
      }

      @tailrec
      def filterCodAdvisersE(codAdvisers: List[ExternalPerson], fList1: List[ExternalPerson]): List[ExternalPerson] = {
        codAdvisers match {
          case Nil => fList1.reverse
          case h :: tail => if (resourcesCheck.externals.externalList.exists(_.id == h.id)) {
            filterCodAdvisersE(tail, ExternalPerson(h.id, h.name, filterAvailabilities(h.availability, List())) :: fList1)
          } else {
            filterCodAdvisersE(tail, ExternalPerson(h.id, h.name, h.availability) :: fList1)
          }
        }
      }

      @tailrec
      def filterAvailabilities(avlList: List[Availability], fAvl: List[Availability]): List[Availability] = {
        avlList match {
          case Nil => fAvl.reverse
          case h :: tail => if ((overLap.start.isAfter(h.start) || overLap.start.isEqual(h.start)) &&
            (overLap.end.isBefore(h.end) || overLap.end.isEqual(h.end))) {

            if (h.start.isBefore(overLap.start) && overLap.end.isBefore(h.end)) {
              filterAvailabilities(tail, Availability(overLap.end, h.end, h.preference) ::
                Availability(h.start, overLap.start, h.preference) :: fAvl)
            } else if (h.start.isBefore(overLap.start)) {
              filterAvailabilities(tail, Availability(h.start, overLap.start, h.preference) :: fAvl)
            } else if (overLap.end.isBefore(h.end)) {
              filterAvailabilities(tail, Availability(overLap.end, h.end, h.preference) :: fAvl)
            } else {
              filterAvailabilities(tail, fAvl)
            }
          } else {
            filterAvailabilities(tail, h :: fAvl)
          }
        }
      }

      val fPresident: Teacher = if (resourcesCheck.teachers.teacherList.exists(_.id == v.president.id)) {
        Teacher(v.president.id, v.president.name, filterAvailabilities(v.president.availability, List()))
      } else {
        Teacher(v.president.id, v.president.name, v.president.availability)
      }

      /*
      val fPresident: Try[Teacher] = if (resourcesCheck.teachers.teacherList.exists(_.id == v.president.get.id)) {
        Try(Teacher(v.president.get.id, v.president.get.name, filterAvailabilities(v.president.get.availability, List())))
      } else {
        Try(Teacher(v.president.get.id, v.president.get.name, v.president.get.availability))
      }

      val fAdviser: Try[Teacher] = if (resourcesCheck.teachers.teacherList.exists(_.id == v.adviser.get.id)) {
        Try(Teacher(v.adviser.id, v.adviser.get.name, filterAvailabilities(v.adviser.get.availability, List())))
      } else {
        Try(Teacher(v.adviser.get.id, v.adviser.get.name, v.adviser.get.availability))
      }

      */

      val fAdviser= if (resourcesCheck.teachers.teacherList.exists(_.id == v.adviser.id)) {
        Teacher(v.adviser.id, v.adviser.name, filterAvailabilities(v.adviser.availability, List()))
      } else {
        Teacher(v.adviser.id, v.adviser.name, v.adviser.availability)
      }

      val coAd: List[Resource] =v.coAdvisers
      val fcodAdvisersT: List[Teacher] = filterCodAdvisersT(checkInstance(coAd).teachers.teacherList, List())
      val fcodAdvisersE: List[ExternalPerson] = filterCodAdvisersE(checkInstance(coAd).externals.externalList, List())

      //val fCod: Resources = Resources(Teachers(fcodAdvisersT), Externals(fcodAdvisersE))

      val fSuper: Externals = Externals(filterCodAdvisersE(v.supervisors.externalList, List()))

      Viva(v.title, v.student, fPresident, fAdviser, fcodAdvisersT++fcodAdvisersE, fSuper)
    }

    vivas match {
      case Nil => fVivas
      case h :: tail => filterVivas(tail, resourcesViva, updateVivaAvailabilities(h, resourcesViva, overlap) :: fVivas, overlap)
    }
  }

  def apply(agenda: Agenda)(): Try[CompleteSchedule] = {
    val res: CompleteSchedule = createCompleteSchedule(agenda)

    val find: Option[ScheduledViva] = res.scheduledVivaList.find(x => x.start == LocalDateTime.MIN)
    find match {
      case Some(sv) => Failure(new Exception("Viva " + sv.viva.title.s + " cannot be scheduled"))
      case None => Success(res)
    }
    /*
    if (res.scheduledVivaList.exists(f => f.start == LocalDateTime.MIN)) {
      val viva: NonEmptyString = res.scheduledVivaList.find(x => x.start == LocalDateTime.MIN).get.viva.title
      Failure(new Exception("Viva " + viva.s + " cannot be scheduled"))
    } else {
      Success(res)
    }*/
  }

}

