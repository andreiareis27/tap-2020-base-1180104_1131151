package assessment

import domain.schedule.{Agenda, CompleteSchedule}
import scala.util.{Failure, Success, Try}
import scala.xml.{Elem, Node}

object ImportScheduleData {

  def apply(n: Elem, algorithm: String): Try[Elem] = {
    val tNode: Try[Node] = (n \\ "agenda").headOption.fold[Try[Node]](Failure(new Exception("node agenda does not exist!")))(Success(_))
    tNode match {
      case Failure(e) => Failure(new Exception(e.getMessage))
      case Success(node) => val tAgenda: Try[Agenda] = Agenda.from(node)
        tAgenda match {
          case Failure(e) => Failure(new Exception(e.getMessage))
          case Success(agenda) =>
            val cs: Try[CompleteSchedule] = algorithm match {
              case "MS01" => Functions(agenda)
              //case "MS03" => FunctionsMs03(agenda)
              case "MS03" =>MS03Algorithm(agenda)
            }
            cs match {
              case Failure(e) => Failure(new Exception(e.getMessage))
              case Success(completeSchedule) => Success(CompleteSchedule.toXML(completeSchedule))
            }
        }
    }

  }

}

