import assessment.FunctionsMs03
import domain.schedule._

import scala.util.{Failure, Success, Try}
import scala.xml.{Elem, Node, XML}

object Main extends App {

  val file = "files/assessment/ms03/test/test_dependencies_in.xml"

  val xml: Elem = XML.loadFile(file)

  val node: Try[Node] = (xml \\ "agenda").headOption.fold[Try[Node]](Failure(new Exception("node agenda does not exist!")))(Success(_))

  val agendaTest: Try[Agenda] = Agenda.from(node.get)

  val agenda = agendaTest.get

  val completeSchedule = FunctionsMs03(agenda)
//  val cs: Elem =CompleteSchedule.toXML(completeSchedule)
}

